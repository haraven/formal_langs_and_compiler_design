#pragma once

#include <string>

bool ContainsOnlyCharacter(const std::string& str, const char& ch);

bool IsNumber(const std::string& str);

bool IsAlphaNumeric(const std::string& str);

template<typename T> void safe_delete(T*& a)
{
    if (a != nullptr)
    {
        delete a;
        a = nullptr;
    }
}

template<typename T> void safe_delete_arr(T*& a)
{
    if (a != nullptr)
    {
        delete[] a;
        a = nullptr;
    }
}