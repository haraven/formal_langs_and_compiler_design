#pragma once

#include <exception>
#include <vector>
#include <string>

namespace hrv
{
    class MyException : public std::exception
    {
    public:
        MyException(const std::vector<std::string>& errors);
        virtual const char* what() const throw();
    private:
        std::string _error_string;
    };
}
