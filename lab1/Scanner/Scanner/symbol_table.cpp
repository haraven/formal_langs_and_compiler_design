#include "symbol_table.h"

size_t hrv::IdentifierTable::Add(const std::string & symbol)
{
    return DoublyLinkedList::Add(symbol);
}

hrv::IdentifierTable & hrv::IdentifierTable::Remove(const std::string & symbol)
{
    if (!Contains(symbol))
        throw std::out_of_range("Identifier " + symbol + " was not found in the symbol table");

    DoublyLinkedList::Remove(symbol);

    return *this;
}

hrv::ConstantTable & hrv::ConstantTable::Remove(const std::string & constant)
{
    if (!Contains(constant))
        throw std::out_of_range("Constant " + constant + " was not found in the symbol table");

    DoublyLinkedList::Remove(constant);

    return *this;
}

hrv::SymbolTable::SymbolTable()
: _constant_table()
, _identifier_table()
{}

size_t hrv::SymbolTable::AddIdentifier(const std::string & symbol)
{
    return _identifier_table.Add(symbol);
}

size_t hrv::SymbolTable::AddConstant(const std::string & symbol)
{
    return _constant_table.Add(symbol);
}

//size_t hrv::SymbolTable::Add(const long & constant)
//{
//    return _constant_table.Add(constant);
//}

hrv::SymbolTable & hrv::SymbolTable::Remove(const std::string & symbol)
{
    _identifier_table.Remove(symbol);

    return *this;
}

//hrv::SymbolTable & hrv::SymbolTable::Remove(const long & constant)
//{
//    _constant_table.Remove(constant);
//
//    return *this;
//}

std::string & hrv::SymbolTable::IdentifierAt(const size_t & index)
{
    if (index >= _identifier_table.Size())
        throw std::out_of_range("Index was out of range, for the identifier table");

    return _identifier_table[index];
}

const std::string & hrv::SymbolTable::IdentifierAt(const size_t & index) const
{
    if (index >= _identifier_table.Size())
        throw std::out_of_range("Index was out of range, for the identifier table");

    return _identifier_table[index];
}

std::string & hrv::SymbolTable::ConstantAt(const size_t & index)
{
    if (index >= _constant_table.Size())
        throw std::out_of_range("Index was out of range, for the constant table");

    return _constant_table[index];
}

const std::string & hrv::SymbolTable::ConstantAt(const size_t & index) const
{
    if (index >= _constant_table.Size())
        throw std::out_of_range("Index was out of range, for the constant table");

    return _constant_table[index];
}

size_t hrv::SymbolTable::TotalSize() const
{
    return _constant_table.Size() + _identifier_table.Size();
}

size_t hrv::SymbolTable::IdentifierCount() const
{
    return _identifier_table.Size();
}

size_t hrv::SymbolTable::ConstantCount() const
{
    return _constant_table.Size();
}

bool hrv::SymbolTable::Contains(const std::string & symbol)
{
    return _identifier_table.Contains(symbol) || _constant_table.Contains(symbol);
}

//bool hrv::SymbolTable::Contains(const long & constant)
//{
//    return _constant_table.Contains(constant);
//}
//
//size_t hrv::ConstantTable::Add(const std::string & constant)
//{
//    return size_t();
//}
