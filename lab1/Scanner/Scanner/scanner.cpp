#include "scanner.h"
#include <boost\tokenizer.hpp>
#include <cctype>
#include <algorithm>
#include "my_exception.h"
#include "my_constants.h"

bool IsStringNotAlnum(const std::string& str)
{
    return std::all_of(str.begin(), str.end(), [](auto character)
    {
        return !std::isalnum(character);
    });
}

bool CheckIdentifier(const std::string& token)
{
    if (std::isdigit(token[0]))
        return false;
    bool is_let_uscore_dig_seq = std::all_of(token.begin(), token.end(), [](auto character)
    {
        return std::isalnum(character) || character == '_';
    });

    if (!is_let_uscore_dig_seq)
        return false;

    if (ContainsOnlyCharacter(token, '_'))
        return false;

    return true;
}

std::vector<std::string> hrv::Scanner::TryFind(const std::string & line, const FindType & find_type)
{
    std::string quotation_mark_or_apostrophe = find_type == FIND_CHARACTER ? "\'" : "\"";

    std::vector<std::string> res;

    size_t pos_begin = line.find_first_of(quotation_mark_or_apostrophe);
    size_t pos_end = line.find_last_of(quotation_mark_or_apostrophe);
    if (pos_begin != pos_end && (pos_begin != 0 || pos_end != line.size() - 1))
    {
        res.push_back(line.substr(0, pos_begin));
        res.push_back(line.substr(pos_begin, pos_end - pos_begin + 1));
        res.push_back(line.substr(pos_end + 1, line.size()));
    }

    return res;
}

std::vector<std::string> hrv::Scanner::IsIdentifier(const std::string & token)
{
    std::vector<std::string> errors;
    if (token.size() > IDENTIFIER_SIZE_MAX)
        errors.push_back("Token " + token + " : identifiers must be of at most " + std::to_string(IDENTIFIER_SIZE_MAX) + " characters");
    if (std::isdigit(token[0]))
        errors.push_back("Token " + token + " : identifiers cannot start with digits");
    bool is_let_uscore_dig_seq = std::all_of(token.begin(), token.end(), [](auto character)
    {
        return std::isalnum(character) || character == '_';
    });

    if (!is_let_uscore_dig_seq)
        errors.push_back("Token " + token + " : identifier contains invalid characters");

    if (ContainsOnlyCharacter(token, '_'))
        errors.push_back("Token " + token + " : identifier cannot be a string of underscores");

    return errors;
}

bool IsString(const std::string& token)
{
    if (token[0] != '"')
        return false;
    if (token.find_last_of('"') != token.size() - 1)
        return false;

    return true;
}

bool IsCharacter(const std::string& token)
{
    if (token[0] != '\'')
        return false;
    if (token.find_last_of('\'') != token.size() - 1)
        return false;
    if (token.size() > 3)
        return false;

    return true;
}

std::vector<std::string> hrv::Scanner::IsConstant(const std::string & token)
{
    std::vector<std::string> errors;

    if (token.length() == 1 && !IsNumber(token))
        errors.push_back("Token " + token + " : invalid character");
    else if (token[0] == '"')
    {
        if (!IsString(token))
            errors.push_back("Token " + token + " : invalid string");
    }
    else if (token[0] == '\'')
    {
        if (!IsCharacter(token))
            errors.push_back("Token " + token + " : invalid character sequence");
    }
    else if (!IsNumber(token))
        errors.push_back("Token " + token + " : invalid constant");

    return errors;
}

bool hrv::Scanner::IsKeyword(const std::string & token)
{
    return _encoding_table.find(token) != _encoding_table.end();
}

std::vector<std::string> hrv::Scanner::HandleToken(const std::string & token)
{
    std::vector<std::string> errors;
    
    if (token.size() == 0)
        return errors;

    LOG(INFO) << "Handling token: " << token;
    if (token[0] != '"' || token[token.size() - 1] != '"')
    {
        size_t semicolon_pos = token.find_first_of(";");
        if (semicolon_pos != std::string::npos && token.size() > 1 && !IsString(token) && !IsCharacter(token))
        {
            std::string before = token.substr(0, semicolon_pos);
            std::string semicolon = token.substr(semicolon_pos, 1);
            std::string after = token.substr(semicolon_pos + 1, token.size());

            std::vector<std::string> tmp = HandleToken(before);
            errors.insert(errors.end(), tmp.begin(), tmp.end());
            tmp = HandleToken(semicolon);
            errors.insert(errors.end(), tmp.begin(), tmp.end());
            tmp = HandleToken(after);
            errors.insert(errors.end(), tmp.begin(), tmp.end());

            return errors;
        }
    }

    if (IsKeyword(token))
    {
        size_t token_code = _encoding_table[token];
        _program_internal_form.push_back(std::pair<size_t, size_t>(token_code, NOT_IN_SYMBOL_TABLE));
    }
    else
    {
        std::vector<std::string> tmp = IsConstant(token);
        if (tmp.size())
        {
            if (!CheckIdentifier(token))
            {
                errors.insert(errors.end(), tmp.begin(), tmp.end());
            }
            else
            {
                tmp = IsIdentifier(token);
                if (tmp.size())
                {
                    errors.insert(errors.end(), tmp.begin(), tmp.end());
                }
                else
                {
                    size_t identifier_pos = _symbol_table.AddIdentifier(token);
                    size_t token_code = _encoding_table[TOKEN_IDENTIFIER];
                    _program_internal_form.push_back(std::pair<size_t, size_t>(token_code, identifier_pos));
                }
            }
        }
        else
        {
            size_t constant_pos = _symbol_table.AddConstant(token);
            size_t token_code = _encoding_table[TOKEN_CONSTANT];
            _program_internal_form.push_back(std::pair<size_t, size_t>(token_code, constant_pos));
        }
    }

    return errors;
}

std::vector<std::string> hrv::Scanner::HandleTokens(const std::string& tokens)
{
    std::vector<std::string> errors;
    LOG(INFO) << "Handling tokens: " << tokens;
    std::vector<std::string> split_string_attempt = TryFind(tokens, FIND_STRING);
    if (!split_string_attempt.size())
        split_string_attempt = TryFind(tokens, FIND_CHARACTER);
    if (split_string_attempt.size())
    {
        std::for_each(split_string_attempt.begin(), split_string_attempt.end(), [&errors, this](auto str)
        {
            std::vector<std::string> tmp = HandleTokens(str);
            errors.insert(errors.end(), tmp.begin(), tmp.end());
        });
    }
    else
    {
        if (tokens[0] == tokens[tokens.size() - 1] && (tokens[0] == '"' || tokens[0] == '\''))
            HandleToken(tokens);
        else
        {
            boost::char_separator<char> separator(SEPARATOR_SPACE);
            boost_tokenizer tokenizer(tokens, separator);

            std::vector<std::string> tmp;
            for (boost_tokenizer::iterator it = tokenizer.begin(); it != tokenizer.end(); ++it)
            {
                tmp = HandleToken(*it);
                errors.insert(errors.end(), tmp.begin(), tmp.end());
            }
        }
    }

    return errors;
}

hrv::Scanner::Scanner(const enc_table_t& encoding_table)
: _encoding_table(encoding_table)
, _program_internal_form()
, _symbol_table()
, _reader()
{}

hrv::Scanner & hrv::Scanner::Scan(const std::string & filename)
{
    _reader.Open(filename, SEPARATOR_NEWLINE);

    std::vector<std::string> errors;

    size_t line_count = 0;
    std::vector<std::string> tmp;
    for (FileReader::Iterator it = _reader.Begin(); it != _reader.End(); ++it, ++line_count)
    {
        LOG(INFO) << "==============================";
        LOG(INFO) << "Scanning line: " << line_count;
        tmp = HandleTokens(*it);
        if (tmp.size())
        {
            errors.push_back("On line " + std::to_string(line_count) + ":");
            errors.insert(errors.end(), tmp.begin(), tmp.end());
        }
        LOG(INFO) << "==============================";
    }

    if (errors.size())
        throw hrv::MyException(errors);

    return *this;
}

const hrv::Scanner::pif_t& hrv::Scanner::GetPIF() const
{
    return _program_internal_form;
}

const hrv::SymbolTable & hrv::Scanner::GetST() const
{
    return _symbol_table;
}

hrv::Scanner::~Scanner()
{
    _encoding_table.clear();
    _program_internal_form.clear();
}
