#pragma once

#include <string>

static const std::string TOKEN_IDENTIFIER = "identifier";
static const std::string TOKEN_CONSTANT = "const";
