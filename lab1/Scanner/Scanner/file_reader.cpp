#include "file_reader.h"
#include <fstream>
#include <logger.hpp>

std::string hrv::FileReader::ReadFile(const char * separator)
{
    if (_filename != NO_FILE)
    {
        std::ifstream file(_filename);
        std::string res = "";
        std::string tmp;
        while (std::getline(file, tmp))
            res += tmp + separator;
        file.close();
        return res;
    }

    return "";
}

hrv::FileReader::FileReader(const std::string & filename, const char* separator)
: _filename(filename)
, _separator(separator)
, _tokenizer(NO_FILE)
{
    _filename  = filename;
    _data      = ReadFile(separator);
    _tokenizer = boost::tokenizer<boost::char_separator<char>>(_data, _separator);
}

hrv::FileReader & hrv::FileReader::Open(const std::string & filename, const char* separator)
{
    _filename  = filename;
    _data      = ReadFile(separator);
    _separator = boost::char_separator<char>(separator);
    _tokenizer = boost::tokenizer<boost::char_separator<char>>(_data, _separator);

    return *this;
}

hrv::FileReader::Iterator hrv::FileReader::Begin()
{
    return _tokenizer.begin();
}

hrv::FileReader::Iterator hrv::FileReader::End()
{
    return _tokenizer.end();
}
