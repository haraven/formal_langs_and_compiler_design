#pragma once

#include <string>
#include "my_list.hpp"

namespace hrv
{
    const static size_t IDENTIFIER_SIZE_MAX = 8;

    class IdentifierTable : public DoublyLinkedList<std::string>
    {
    private:
    public:

        size_t Add(const std::string& symbol) final;
        hrv::IdentifierTable& Remove(const std::string& symbol) final;
    };

    class ConstantTable : public DoublyLinkedList<std::string>
    {
    public:
        hrv::ConstantTable& Remove(const std::string& constant) final;
    };

    class SymbolTable
    {
    public:
        SymbolTable();

        size_t AddIdentifier(const std::string& symbol);
        size_t AddConstant(const std::string& symbol);
        //size_t Add(const long& constant);

        hrv::SymbolTable& Remove(const std::string& symbol);
        //hrv::SymbolTable& Remove(const long& constant);

        std::string& IdentifierAt(const size_t& index);
        const std::string& IdentifierAt(const size_t& index) const;
        std::string& ConstantAt(const size_t& index);
        const std::string& ConstantAt(const size_t& index) const;

        size_t TotalSize() const;
        size_t IdentifierCount() const;
        size_t ConstantCount() const;

        bool Contains(const std::string& symbol);
        //bool Contains(const long& constant);

    private:
        IdentifierTable _identifier_table;
        ConstantTable _constant_table;
    };
}
