#pragma once

#include <vector>
#include <utility>
#include "file_reader.h"
#include "symbol_table.h"

namespace hrv
{
    class Scanner
    {
    private:
        typedef boost::tokenizer<boost::char_separator<char>> boost_tokenizer;

        const static size_t NOT_IN_SYMBOL_TABLE = 0xFFFFFFFF;
        const char* SEPARATOR_NEWLINE = "\r\n";
        const char* SEPARATOR_SPACE   = " ";

        enum FindType
        {
            FIND_CHARACTER,
            FIND_STRING,
            FIND_COMPOUND_STATEMENT
        };

        std::vector<std::string> TryFind(const std::string& line, const FindType& find_type);
        std::vector<std::string> IsIdentifier(const std::string& token);
        std::vector<std::string> IsConstant(const std::string& token);
        bool IsKeyword(const std::string& token);

        std::vector<std::string> HandleToken(const std::string& token);
        std::vector<std::string> HandleTokens(const std::string& tokens);
    public:
        typedef std::pair<size_t, size_t> token_symbol_pair_t;
        typedef std::vector<token_symbol_pair_t> pif_t;
        typedef std::map<std::string, size_t> enc_table_t;

        Scanner(const enc_table_t& encoding_table);

        hrv::Scanner& Scan(const std::string& filename);

        const pif_t& GetPIF() const;
        const hrv::SymbolTable& GetST() const;

        ~Scanner();
    private:
        std::map<std::string, size_t> _encoding_table;
        pif_t _program_internal_form;
        SymbolTable _symbol_table;
        FileReader _reader;
    };
}

