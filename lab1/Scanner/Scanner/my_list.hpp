#pragma once

#include <vector>
#include <algorithm>
#include <logger.hpp>
#include "utils.h"

namespace hrv
{
    template<typename T>
    class DoublyLinkedList
    {
    private:
        struct Node
        {
            Node(const T& val, Node* next, Node* prev);

            T val;
            Node* next;
            Node* prev;
        };

    public:
        const static size_t INVALID_POS = 0xFFFFFFFF;

        DoublyLinkedList();

        virtual size_t Add(const T& val);
        virtual hrv::DoublyLinkedList<T>& Remove(const T& val);
        T& At(size_t index);
        const T& At(size_t index) const;
        T& OrderedAt(size_t index);
        const T& OrderedAt(size_t index) const;
        T& operator[](size_t index);
        const T& operator[](size_t index) const;
        bool Contains(const T& val);

        const size_t Size() const;

        ~DoublyLinkedList();
    private:
        std::vector<Node*> _container;
        Node* _head;
    };

    template<typename T>
    hrv::DoublyLinkedList<T>::DoublyLinkedList()
    : _head(nullptr)
    {}

    template<typename T>
    size_t hrv::DoublyLinkedList<T>::Add(const T& val)
    {
        size_t index = 0;
        if (_head == nullptr)
        {
            _container.push_back(new Node(val, nullptr, nullptr));
            _head = _container[0];
        }
        else
        {
            for (size_t i = 0; i < _container.size(); ++i)
                if (_container[i]->val == val)
                    return i;

            //if (std::find_if(_container.begin(), _container.end(), [&val](auto node)
            //{
            //    return node->val == val;
            //}) != _container.end())
            //    return INVALID_POS;

            Node* node = _head, *before_last = nullptr;
            for (; node != nullptr; node = node->next, ++index)
                if (val < node->val)
                    break;
                else
                    before_last = node;

            _container.push_back(new Node(val, node, before_last));
            if (node)
            {
                if (node->prev == nullptr)
                    _head = _container[_container.size() - 1];
                else
                    node->prev->next = _container[_container.size() - 1];
                node->prev = _container[_container.size() - 1];
            }
            else
                before_last->next = _container[_container.size() - 1];
        }

        return _container.size() - 1;
    }

    template<typename T>
    hrv::DoublyLinkedList<T>& hrv::DoublyLinkedList<T>::Remove(const T& val)
    {
        for (size_t i = 0; i < _container.size(); ++i)
        {
            Node* node = _container[i];
            if (node->val == val)
            {
                if (node == _head)
                    _head = node->next;
                safe_delete(node);
                _container.erase(_container.begin() + i);
                break;
            }
        }

        return *this;
    }

    template<typename T>
    T& hrv::DoublyLinkedList<T>::At(size_t index)
    {
        return _container[index]->val;
        //Node* node = _head;
        //for (size_t i = 0; i != index && node->next; ++i)
        //    node = node->next;
        //return node->val;
    }

    template<typename T>
    inline const T & DoublyLinkedList<T>::At(size_t index) const
    {
        return _container[index]->val;
    }

    template<typename T>
    inline T & DoublyLinkedList<T>::OrderedAt(size_t index)
    {
        for (Node* node = _head; node->next != nullptr; node = node->next, --index)
            if (index == 0)
                return node->val;
    }

    template<typename T>
    inline const T & DoublyLinkedList<T>::OrderedAt(size_t index) const
    {
        for (Node* node = _head; node->next != nullptr; node = node->next, --index)
            if (index == 0)
                return node->val;
    }

    template<typename T>
    T& hrv::DoublyLinkedList<T>::operator[](size_t index)
    {
        return At(index);
    }

    template<typename T>
    inline const T & DoublyLinkedList<T>::operator[](size_t index) const
    {
        return At(index);
    }

    template<typename T>
    inline bool DoublyLinkedList<T>::Contains(const T & val)
    {
        for (Node* node = _head; node != nullptr; node = node->next)
            if (node->val == val)
                return true;
        return false;
    }

    template<typename T>
    const size_t hrv::DoublyLinkedList<T>::Size() const
    {
        return _container.size();
    }

    template<typename T>
    hrv::DoublyLinkedList<T>::~DoublyLinkedList()
    {
        std::for_each(_container.begin(), _container.end(), [](auto node)
        {
            safe_delete(node);
        });

        _container.clear();
    }

    template<typename T>
    hrv::DoublyLinkedList<T>::Node::Node(const T& val, Node* next, Node* prev)
        : val(val)
        , next(next)
        , prev(prev)
    {}

}