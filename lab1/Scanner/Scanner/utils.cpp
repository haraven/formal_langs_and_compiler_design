#include "utils.h"
#include <algorithm>
#include <cctype>

bool ContainsOnlyCharacter(const std::string & str, const char & ch)
{
    return std::all_of(str.begin(), str.end(), [&ch](auto character)
    {
        return character == ch;
    });
}

bool IsNumber(const std::string& str)
{
    int start = str[0] == '-' || str[0] == '+' ? 1 : 0;
    bool starts_with_zero = str[0] != '-' && str[0] != '+' ? (str[0] == '0' ? true : false) : (str[1] == '0' ? true : false);
    return !str.empty() && std::find_if(str.begin() + start, str.end(), [](char c) { return !std::isdigit(c); }) == str.end() && !starts_with_zero;
}

bool IsAlphaNumeric(const std::string& str)
{
    return std::all_of(str.begin(), str.end(), [](auto character)
    {
        return std::isalnum(character);
    });
}