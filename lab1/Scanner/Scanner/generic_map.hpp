#pragma once

#include <map>

namespace hrv
{
    template<typename K, typename V>
    class GenericMap
    {
    public:
        GenericMap();

        hrv::GenericMap<K, V>& Add(const K& key, const V& value);

        hrv::GenericMap<K, V>& RemoveVal(const V& value);
        hrv::GenericMap<K, V>& RemoveKey(const K& key);
        V& ValueOf(const K& key);
        V& operator[](const K& key);

        ~GenericMap();
    private:
        std::map<K, V> _container;
    };
    template<typename K, typename V>
    inline GenericMap<K, V>::GenericMap()
    : _container()
    {}

    template<typename K, typename V>
    inline hrv::GenericMap<K, V> & GenericMap<K, V>::Add(const K & key, const V & value)
    {
        std::map<K, V>::iterator it = _container.find(key);
        if (it != _container.end())
            _container[key] = value;
        else
            _container[key] = value;

        return *this;
    }

    template<typename K, typename V>
    inline hrv::GenericMap<K, V> & GenericMap<K, V>::RemoveVal(const V & value)
    {
        for (std::map<K, V>::iterator it = _container.begin(); it != _container.end(); ++it)
            if (*it == value)
                _container.erase(it);

        return *this;
    }

    template<typename K, typename V>
    inline hrv::GenericMap<K, V> & GenericMap<K, V>::RemoveKey(const K & key)
    {
        _container.erase(_container.find(value));

        return *this;
    }

    template<typename K, typename V>
    V & GenericMap<K, V>::ValueOf(const K & key)
    {
        return _container[key];
    }
    template<typename K, typename V>
    inline V & GenericMap<K, V>::operator[](const K & key)
    {
        return ValueOf(key);
    }
    template<typename K, typename V>
    inline GenericMap<K, V>::~GenericMap()
    {
        _container.clear();
    }
}
