#pragma once

#include <vector>
#include <string>
#include <boost\tokenizer.hpp>

namespace hrv 
{
    static const std::string NO_FILE = "";
    class FileReader
    {
    private:
        std::string ReadFile(const char* separator);
    public:
        typedef boost::tokenizer<boost::char_separator<char>>::iterator Iterator;
        
        FileReader(const std::string& filename = NO_FILE, const char* separator = " ");

        hrv::FileReader& Open(const std::string& filename, const char* separator = " ");
            
        Iterator Begin();
        Iterator End();

        ~FileReader() {}
    private:
        std::string _data;
        boost::tokenizer<boost::char_separator<char>> _tokenizer;
        boost::char_separator<char> _separator;
        std::string _filename;
    };
}