#define ELPP_STL_LOGGING
#include <logger.hpp>
INITIALIZE_EASYLOGGINGPP

#include<string>
#include <map>
#include <algorithm>
#include "my_constants.h"
#include "scanner.h"
#include "my_exception.h"

enum RetCode
{
    SUCCESS = 0,
    FAILURE = -1
};

hrv::Scanner::enc_table_t InitEncodingTable()
{
    std::map<std::string, size_t> table;
    size_t index = 0;

    table[TOKEN_IDENTIFIER] = index++;
    table[TOKEN_CONSTANT]   = index++;
    table["int_main()"]     = index++;
    table["int"]            = index++;
    table["string"]         = index++;
    table["char"]           = index++;
    table["if"]             = index++;
    table["else"]           = index++;
    table["in"]             = index++;
    table["while"]          = index++;
    table["for"]            = index++;
    table["write"]          = index++;
    table["read"]           = index++;
    table["ret"]            = index++;
    table["+"]              = index++;
    table["-"]              = index++;
    table["*"]              = index++;
    table["/"]              = index++;
    table["="]              = index++;
    table["<"]              = index++;
    table["<="]             = index++;
    table["=="]             = index++;
    table["!="]             = index++;
    table[">"]              = index++;
    table[">="]             = index++;
    table["&&"]             = index++;
    table["||"]             = index++;
    table["("]              = index++;
    table[")"]              = index++;
    table["{"]              = index++;
    table["}"]              = index++;
    table["["]              = index++;
    table["]"]              = index++;
    table[":"]              = index++;
    table[";"]              = index++;
    table[","]              = index++;

    return table;
}

int main() 
{
    el::Loggers::configureFromGlobal("logging.conf");

    const std::string my_file = "test.cpp";

    hrv::Scanner::enc_table_t encoding_table = InitEncodingTable();
    hrv::Scanner scanner(encoding_table);

    try
    {
        scanner.Scan(my_file);
        const hrv::Scanner::pif_t& pif = scanner.GetPIF();
        LOG(INFO) << "------------------------------------------------------";
        LOG(INFO) << "PIF contents";
        std::for_each(pif.cbegin(), pif.cend(), [](hrv::Scanner::token_symbol_pair_t pair)
        {
            LOG(INFO) << "Token code: " << pair.first << "; symbol table position: " << pair.second;
        });
        LOG(INFO) << "------------------------------------------------------";
        LOG(INFO) << "Symbol table contents";
        const hrv::SymbolTable& sym_table = scanner.GetST();
        LOG(INFO) << "Constants table:";
        for (size_t i = 0; i < sym_table.ConstantCount(); ++i)
            LOG(INFO) << sym_table.ConstantAt(i);
        LOG(INFO) << "Identifiers table:";
        for (size_t i = 0; i < sym_table.IdentifierCount(); ++i)
            LOG(INFO) << sym_table.IdentifierAt(i);
        LOG(INFO) << "------------------------------------------------------";
    }
    catch (hrv::MyException& e)
    {
        LOG(ERROR) << e.what();
        return FAILURE;
    }

    return SUCCESS;
}