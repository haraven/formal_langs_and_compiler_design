#include "my_exception.h"
#include <algorithm>

hrv::MyException::MyException(const std::vector<std::string>& errors)
: _error_string("")
{
    std::for_each(errors.cbegin(), errors.cend(), [this](auto str)
    {
        _error_string += str + "\r\n";
    });
}

const char * hrv::MyException::what() const throw()
{
    return _error_string.c_str();
}
