#include "automaton.h"
#include <algorithm>
#include "utils.h"

bool hrv::compiler::Automaton::ContainsInitialState()
{
    return std::any_of(_states.begin(), _states.end(), [](auto state)
    {
        return state.is_initial_state;
    });
}

hrv::compiler::Automaton::Automaton()
{}

hrv::compiler::Automaton & hrv::compiler::Automaton::AddState(const State & state)
{
    if (state.is_initial_state && ContainsInitialState())
        throw std::out_of_range("Automaton already contains an initial state. Could not add state " + state.val);

    _states.insert(state);

    return *this;
}

hrv::compiler::Automaton & hrv::compiler::Automaton::AddSymbolToAlphabet(const std::string & symbol)
{
    _alphabet.insert(symbol);

    return *this;
}

hrv::compiler::Automaton & hrv::compiler::Automaton::AddTransition(const hrv::compiler::Automaton::TransitionArg & arg, const hrv::compiler::Automaton::TransitionRes & res)
{
    auto it = _transition_fn.find(arg);
    if (it != _transition_fn.end())
        it->second.insert(res.begin(), res.end());
    else
        _transition_fn[arg] = res;

    return *this;
}

bool hrv::compiler::Automaton::ContainsState(const std::string & state_str) const
{
    return std::find_if(_states.cbegin(), _states.cend(), [&state_str](auto state)
    {
        return state.val == state_str;
    }) != _states.cend();;
}

const hrv::compiler::State & hrv::compiler::Automaton::GetInitialState() const
{
    std::set<State>::iterator res = std::find_if(_states.begin(), _states.end(), [](auto state)
    {
        return state.is_initial_state;
    });

    if (res == _states.end())
        throw std::runtime_error("No initial state found");

    return *res;
}

const std::set<hrv::compiler::State> hrv::compiler::Automaton::GetFinalStates() const
{
    std::set<State> final_states;

    for (auto state : _states)
        if (state.is_final_state)
            final_states.insert(state);

    return final_states;
}

hrv::compiler::Automaton & hrv::compiler::Automaton::Clear()
{
    _states.clear();
    _alphabet.clear();
    _transition_fn.clear();

    return *this;
}

hrv::compiler::Automaton::~Automaton()
{}

bool hrv::compiler::operator<(const hrv::compiler::State & left, const hrv::compiler::State & right)
{
    return left.val < right.val;
}

hrv::compiler::AutomatonReader::AutomatonReader(const std::string & filename)
: FileReader(filename, SEPARATOR_NEWLINE)
, _state_indicator("# STATES")
, _alphabet_indicator("# ALPHABET")
, _transition_fn_indicator("# TRANSITION")
, _finished_indicator("# FINISHED")
{}

hrv::compiler::Automaton hrv::compiler::AutomatonReader::GetAutomaton()
{
    if (_filename == NO_FILE)
        throw std::runtime_error("Could not retrieve grammar from file. Reason: no file was opened");

    Automaton a;

    Iterator line_it = Begin();
    HandleLines(line_it, a);

    return a;
}

std::set<hrv::compiler::State> hrv::compiler::AutomatonReader::GetStatesFromStream(std::istream & in)
{
    std::set<State> states;
    std::string line;
    do
    {
        std::getline(in, line);
        if (line == _finished_indicator || line == _state_indicator || line == _alphabet_indicator || line == _transition_fn_indicator)
            break;
        if (line == "")
            continue;
        states.insert(HandleState(line));
    } while (line != _finished_indicator & line != _alphabet_indicator && line != _transition_fn_indicator);

    return states;
}

std::set<std::string> hrv::compiler::AutomatonReader::GetAlphabetFromStream(std::istream & in)
{
    std::set<std::string> alphabet;
    std::string line;
    do
    {
        std::getline(in, line);
        if (line == _finished_indicator || line == _alphabet_indicator || line == _state_indicator || line == _transition_fn_indicator)
            break;
        if (line == "")
            continue;
        std::set<std::string> tmp = HandleAlphabet(line);
        alphabet.insert(tmp.begin(), tmp.end());
    } while (line != _finished_indicator & line != _state_indicator && line != _transition_fn_indicator);

    return alphabet;
}

hrv::compiler::AutomatonReader& hrv::compiler::AutomatonReader::GetTransitionFnFromStream(std::istream & in, hrv::compiler::Automaton & a)
{
    std::string line;
    do
    {
        std::getline(in, line);
        if (line == _finished_indicator || line == _alphabet_indicator || line == _state_indicator || line == _transition_fn_indicator)
            break;
        if (line == "")
            continue;
        HandleTransition(line, a);
    } while (line != _finished_indicator & line != _state_indicator && line != _alphabet_indicator);

    return *this;
}

void hrv::compiler::AutomatonReader::HandleLines(Iterator & line_it, hrv::compiler::Automaton & a)
{
    if (line_it == End())
        return;

    std::string line = *line_it;
    if (line == _state_indicator)
    {
        ++line_it;
        if (line_it != End())
            line = *line_it;
        else return;

        while (line != _alphabet_indicator && line != _transition_fn_indicator && line != _state_indicator && line_it != End())
        {
            HandleState(line, a);
            ++line_it;
            if (line_it != End())
                line = *line_it;
            else return;
        }
    }

    if (line == _alphabet_indicator)
    {
        ++line_it;
        if (line_it != End())
            line = *line_it;
        else return;

        while (line != _alphabet_indicator && line != _transition_fn_indicator && line != _state_indicator && line_it != End())
        {
            HandleSymbol(line, a);
            ++line_it;
            if (line_it != End())
                line = *line_it;
            else return;
        }
    }

    if (line == _transition_fn_indicator)
    {
        ++line_it;
        if (line_it != End())
            line = *line_it;
        else return;

        while (line != _alphabet_indicator && line != _transition_fn_indicator && line != _state_indicator && line_it != End())
        {
            HandleTransition(line, a);
            ++line_it;
            if (line_it != End())
                line = *line_it;
            else return;
        }
    }
}

void hrv::compiler::AutomatonReader::HandleState(const std::string & line, hrv::compiler::Automaton & a)
{
    if (line == "")
        return;

    State s;
    std::string trimmed_line = utils::TrimCharacter(line, ' ');
    std::vector<std::string> split_line = utils::Split(trimmed_line, ',');
    if (split_line.size() < 3)
        return;
    if (split_line.size() > 3) throw std::runtime_error("Error parsing automaton file \"" + _filename + "\": line \"" + line + "\" doesn't contain a valid state");
    s.is_initial_state = split_line[1] == STRING_TRUE ? true : false;
    s.is_final_state = split_line[2] == STRING_TRUE ? true : false;

    s.val = split_line[0];
    a.AddState(s);
}

void hrv::compiler::AutomatonReader::HandleSymbol(const std::string & line, hrv::compiler::Automaton & a)
{
    if (line == "")
        return;

    std::string trimmed_line = utils::TrimCharacter(line, ' ');
    std::vector<std::string> split_line = utils::Split(trimmed_line, ',');
    if (split_line.size() == 0)
        a.AddSymbolToAlphabet(trimmed_line);
    else for (auto token : split_line)
        a.AddSymbolToAlphabet(token);
}

void hrv::compiler::AutomatonReader::HandleTransition(const std::string & line, hrv::compiler::Automaton & a)
{
    if (line == "")
        return;

    std::string trimmed_line = utils::TrimCharacter(line, ' ');
    std::vector<std::string> split_line = utils::Split(trimmed_line, ':');
    if (split_line.size() != 2)
        throw std::runtime_error("Error parsing automaton file \"" + _filename + "\": line " + line + "\" doesn't contain a valid transition");

    std::vector<std::string> split_lhs = utils::Split(split_line[0], ',');
    if (split_lhs.size() != 2)
        throw std::runtime_error("Error parsing automaton file \"" + _filename + "\": line " + line + "\" doesn't contain a valid transition");
    
    auto it = std::find_if(a.GetStates().cbegin(), a.GetStates().cend(), [&split_lhs](auto state)
    {
        return split_lhs[0] == state.val;
    });
    if (it == a.GetStates().cend())
        throw std::runtime_error("Error parsing automaton file \"" + _filename + "\": line " + line + "\" doesn't contain a valid transition");

    State s = *it;
    std::string symbol = split_lhs[1];
    Automaton::TransitionArg arg(s, symbol);

    std::vector<std::string> split_rhs = utils::Split(split_line[1], ',');
    Automaton::TransitionRes res;
    std::for_each(split_rhs.begin(), split_rhs.end(), [&line, this, &a, &res](auto state_str)
    {
        auto it = std::find_if(a.GetStates().begin(), a.GetStates().end(), [&state_str](auto state)
        {
            return state.val == state_str;

        });
        if (it == a.GetStates().end())
            throw std::runtime_error("Error parsing automaton file \"" + _filename + "\": line " + line + "\" contains a state not found in the automaton");

        res.insert(*it);
    });

    a.AddTransition(arg, res);
}

hrv::compiler::State hrv::compiler::AutomatonReader::HandleState(const std::string & line)
{
    State s;
    if (line == "")
        return s;

    std::string trimmed_line = utils::TrimCharacter(line, ' ');
    std::vector<std::string> split_line = utils::Split(trimmed_line, ',');
    if (split_line.size() < 3)
        return s;
    if (split_line.size() > 3) throw std::runtime_error("Error parsing automaton file \"" + _filename + "\": line \"" + line + "\" doesn't contain a valid state");
    s.is_initial_state = split_line[1] == STRING_TRUE ? true : false;
    s.is_final_state = split_line[2] == STRING_TRUE ? true : false;

    s.val = split_line[0];

    return s;
}

std::set<std::string> hrv::compiler::AutomatonReader::HandleAlphabet(const std::string & line)
{
    std::set<std::string> alphabet;
    if (line == "")
        return alphabet;

    std::string trimmed_line = utils::TrimCharacter(line, ' ');
    std::vector<std::string> split_line = utils::Split(trimmed_line, ',');
    if (split_line.size() == 0)
        alphabet.insert(trimmed_line);
    else for (auto token : split_line)
        alphabet.insert(token);

    return alphabet;
}

std::string hrv::compiler::State::ToString() const
{
    return val + (is_initial_state ? ": initial" : "") + (is_final_state ? ": final" : "");
}
