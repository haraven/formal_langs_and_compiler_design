#pragma once

#include <vector>
#include <string>
#include <boost\tokenizer.hpp>

namespace hrv 
{
    namespace io
    {
        static const std::string NO_FILE      = "";
        static const char SEPARATOR_NEWLINE[] = "\n";
        static const char SEPARATOR_SPACE[]   = " ";
        static const char STRING_TRUE[]       = "true";
        static const char STRING_FALSE[]      = "false";
        class FileReader
        {
        private:
            std::string ReadFile(const char* separator);
        public:
            typedef boost::tokenizer<boost::char_separator<char>>::iterator Iterator;

            FileReader(const std::string& filename = NO_FILE, const char* separator = SEPARATOR_SPACE);

            hrv::io::FileReader& Open(const std::string& filename, const char* separator = SEPARATOR_SPACE);

            Iterator Begin();
            Iterator End();

            hrv::io::FileReader& Close();

            ~FileReader();
        protected:
            std::string _data;
            boost::tokenizer<boost::char_separator<char>> _tokenizer;
            boost::char_separator<char> _separator;
            std::string _filename;
        };
    }
}