#pragma once

#include <set>
#include <map>
#include <string>
#include "file_reader.h"

namespace hrv
{
    using namespace io;
    namespace compiler
    {
        class State
        {
        public:
            State() {}
            State(const std::string& val, const bool& is_initial_state = false, const bool& is_final_state = false)
                : val(val)
                , is_initial_state(is_initial_state)
                , is_final_state(is_final_state)
            {}

            friend bool operator<(const hrv::compiler::State& left, const hrv::compiler::State& right);

            std::string ToString() const;

            std::string val;
            bool is_initial_state;
            bool is_final_state;
        };

        const static std::string AUTOMATON_NONE = "none";

        class Automaton
        {
        private:
            bool ContainsInitialState();
        public:
            typedef std::pair<hrv::compiler::State, std::string> TransitionArg;
            typedef std::set<hrv::compiler::State> TransitionRes;
            typedef std::map<TransitionArg, TransitionRes> TransitionFnMap;
            Automaton();

            // also used to insert initial/final states
            hrv::compiler::Automaton& AddState(const State& state);
            hrv::compiler::Automaton& AddSymbolToAlphabet(const std::string& symbol);
            hrv::compiler::Automaton& AddTransition(const hrv::compiler::Automaton::TransitionArg& arg, const hrv::compiler::Automaton::TransitionRes& res);
            bool ContainsState(const std::string& state_str) const;

            const std::set<hrv::compiler::State>& GetStates() const { return _states; }
            const std::set<std::string>& GetAlphabet() const { return _alphabet; }
            const hrv::compiler::State& GetInitialState() const;
            const std::set<hrv::compiler::State> GetFinalStates() const;
            const TransitionFnMap& GetTransitionFn() const { return _transition_fn; }

            hrv::compiler::Automaton& Clear();

            ~Automaton();
        private:
            std::set<State> _states;
            std::set<std::string> _alphabet;
            std::map<TransitionArg, TransitionRes> _transition_fn;
        };

        class AutomatonReader : public FileReader
        {
        private:
            void HandleLines(Iterator& line_it, hrv::compiler::Automaton& a);
            void HandleState(const std::string& line, hrv::compiler::Automaton& a);
            void HandleSymbol(const std::string& line, hrv::compiler::Automaton& a);
            void HandleTransition(const std::string& line, hrv::compiler::Automaton& a);

            hrv::compiler::State HandleState(const std::string& line);
            std::set<std::string> HandleAlphabet(const std::string& line);
        public:
            AutomatonReader(const std::string& filename = NO_FILE);

            hrv::compiler::Automaton GetAutomaton();
            std::set<hrv::compiler::State> GetStatesFromStream(std::istream& in);
            std::set<std::string> GetAlphabetFromStream(std::istream& in);
            hrv::compiler::AutomatonReader& GetTransitionFnFromStream(std::istream& in, hrv::compiler::Automaton& a);
        private:
            std::string _state_indicator;
            std::string _alphabet_indicator;
            std::string _transition_fn_indicator;
            std::string _finished_indicator;
        };
    }
}

