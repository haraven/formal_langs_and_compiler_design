#pragma once
#include <stack>
#include <string>
#include "grammar.h"

namespace hrv
{
    namespace compiler
    {
        enum ParserConfigurationState
        {
            Q, B, T, E
        };

        struct WorkStackItem
        {
            const static size_t INVALID_INDEX = -1;
            WorkStackItem(std::string val, size_t production_index);

            std::string val;
            size_t production_index;
        };

        struct ParserConfiguration
        {
            ParserConfiguration() {}

            void Init(const Grammar& g);

            std::string ToString() const;

            ParserConfigurationState state;
            size_t index;
            std::stack<WorkStackItem> work_stack;
            std::stack<std::string> input_stack;
        };

        class Parser
        {
        private:
            hrv::compiler::Parser& Expand();
            hrv::compiler::Parser& Advance();
            hrv::compiler::Parser& MomentaryInsuccess();
            hrv::compiler::Parser& AnotherTry();
            hrv::compiler::Parser& Back();
            hrv::compiler::Parser& Success();

            std::string ConstructProductionString();
            std::vector<std::string> ReconstructSequenceFromWorkStack();

            std::string ParseInternal();
        public:
            Parser();
            Parser(const Grammar& g);

            std::string Parse(std::vector<std::string> input_sequence);

            hrv::compiler::Parser& SetGrammar(const Grammar& g);
        private:
            Grammar _grammar;
            ParserConfiguration _config;
            std::vector<std::string> _input_sequence;
        };
    }
}