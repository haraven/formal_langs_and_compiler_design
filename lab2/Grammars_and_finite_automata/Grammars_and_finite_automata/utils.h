#pragma once

#include <string>
#include <algorithm>
#include <vector>
#include <sstream>
#include <cctype>

namespace hrv
{
    namespace utils
    {
        template<typename T>
        void SafeDelete(T*& a)
        {
            if (a != nullptr)
            {
                delete a;
                a = nullptr;
            }
        }

        template<typename T>
        void SafeDeleteArr(T*& a)
        {
            if (a != nullptr)
            {
                delete[] a;
                a = nullptr;
            }
        }

        void Split(const std::string &s, char delim, std::vector<std::string> &elems);

        std::vector<std::string> Split(const std::string &s, char delim);
        std::vector<std::string> SplitNoNeighboringDelims(const std::string&s, char delim);

        std::string TrimCharacter(const std::string& s, char ch);

        std::string RandomStr(size_t length);

        template<typename T>
        std::string VectorToString(const std::vector<T>& vec)
        {
            std::stringstream ss;
            for (auto elem : vec)
                ss << elem;

            return ss.str();
        }
    }
}