#pragma once
#include "automaton.h"
#include "grammar.h"

namespace hrv
{
    namespace compiler
    {
        class MyConverter
        {
        private:
            // pair.first is the non-term, while pair.second is the term, in a regular grammar. Unexpected behavior if not regular
            static std::pair<std::string, std::string> SplitProductionRHS(Grammar& grammar, const hrv::compiler::ProductionExpression & expr);
            static std::vector<std::vector<std::string>> CreateProductionRHS(const hrv::compiler::Automaton& automaton, const std::string terminal, const hrv::compiler::Automaton::TransitionRes& results);
            static std::string hrv::compiler::MyConverter::RandomSymbol(Grammar& grammar);
        public:
            MyConverter();

            static hrv::compiler::Automaton RegularGrammarToAutomaton(hrv::compiler::Grammar& grammar);
            static hrv::compiler::Grammar FiniteAutomatonToRegularGrammar(hrv::compiler::Automaton& automaton);

            ~MyConverter();
        };
    }
}

