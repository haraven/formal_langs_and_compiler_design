#pragma once

#include <string>
#include <vector>
#include "grammar.h"
#include "automaton.h"
#include "parser.h"

namespace hrv
{
    namespace ui
    {
        static const char INVALID_MENU_ITEM_NAME[] = "N/A";
        static const char MENU_ITEM_DEFAULT_PREFIX[] = "*";

        class UIElement
        {
        public:
            virtual UIElement& Parse(std::ostream& out) = 0;
        };

        class MenuItem;

        class MenuItemBuilder
        {
        public:
            MenuItemBuilder();

            MenuItemBuilder& Name(const std::string& name);
            MenuItemBuilder& Prefix(const std::string& prefix);
            MenuItem* Build();

        private:
            std::string _prefix;
            std::string _name;
        };

        class MenuItem : public UIElement
        {
        public:
            MenuItem(const std::string& name, const std::string& prefix = MENU_ITEM_DEFAULT_PREFIX);

            static hrv::ui::MenuItemBuilder& Builder();

            virtual MenuItem& Parse(std::ostream& out) final;
        private:
            std::string _prefix;
            std::string _name;
        };

        class Menu : public UIElement
        {
        public:
            Menu();
            virtual Menu& Parse(std::ostream& out) final;

            Menu& Add(UIElement* item);

            ~Menu();
        private:
            std::vector<UIElement*> _menu_items;
        };

        class Interface
        {
        private:
            void InitMainMenu();
            void InitReadMenu();
            void InitDisplayGrammarMenu();
            void InitDisplayFAMenu();
            void HandleExceptionPtr(std::exception_ptr eptr);

            enum Command
            {
                CMD_EXIT = 0,
                CMD_HELP,
                CMD_READ_GRAMMAR,
                CMD_DISPLY_GRAMMAR,
                CMD_IS_GRAMMAR_REGULAR,
                CMD_IS_GRAMMAR_CF,
                CMD_ELIMINATE_INACCESSIBLE_SYMBOLS,
                CMD_READ_FA,
                CMD_DISPLAY_FA,
                CMD_GRAMMAR_TO_FA,
                CMD_FA_TO_GRAMMAR,
                CMD_STDIN,
                CMD_FILE,
                CMD_TERMINALS,
                CMD_NON_TERMINALS,
                CMD_PRODUCTIONS,
                CMD_PRODUCTIONS_OF_NON_TERMINAL,
                CMD_STATES,
                CMD_ALPHABET,
                CMD_TRANSITIONS,
                CMD_FINAL_STATES,
                CMD_PARSE_INPUT,
                CMD_CANCEL,
                CMD_UNKNOWN
            };

            Command InputToCommand(const std::string& input);
        public:
            Interface(std::ostream& out, std::istream& in);
            hrv::ui::Interface& Init();

            Interface& Show();

            ~Interface();
        private:
            compiler::Grammar _grammar;
            compiler::Automaton _finite_automaton;
            compiler::Parser _parser;

            std::istream& _in;
            std::ostream& _out;
            std::string _cmd;
            Menu* _main_menu;
            Menu* _read_menu;
            Menu* _display_grammar_menu;
            Menu* _display_fa_menu;
        };
    }
}