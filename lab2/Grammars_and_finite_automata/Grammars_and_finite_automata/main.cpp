#include <iostream>
#include <logger.hpp>
INITIALIZE_EASYLOGGINGPP
#include "interface.h"
#include "utils.h"

using namespace hrv::ui;

enum RetCode
{
    SUCCESS = 0,
    FAILURE = -1
};

int main()
{
    Interface ui(std::cout, std::cin);

    ui.Init().Show();

    return SUCCESS;
}
