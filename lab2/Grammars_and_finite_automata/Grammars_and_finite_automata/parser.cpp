#include "parser.h"
#include <logger.hpp>

void hrv::compiler::ParserConfiguration::Init(const Grammar & g)
{
    state = Q;
    index = 1;
    input_stack.push(g.GetStartingSymbol().val);
}

std::string hrv::compiler::ParserConfiguration::ToString() const
{
    std::string res = "(";

    switch (state)
    {
    case E:
        res += "E";
        break;
    case Q:
        res += "Q";
        break;
    case T:
        res += "T";
        break;
    case B:
        res += "B";
        break;
    default:
        break;
    }
    res += ", ";
    res += std::to_string(index);
    res += ", { ";

    std::stack<WorkStackItem> work_stack_copy = work_stack;
    if (!work_stack_copy.empty())
    {
        std::vector<std::string> elems;
        while (!work_stack_copy.empty())
        {
            auto work_stack_elem = work_stack_copy.top();
            std::string str;
            str += work_stack_elem.val;
            if (work_stack_elem.production_index != WorkStackItem::INVALID_INDEX)
                str += std::to_string(work_stack_elem.production_index);
            elems.insert(elems.begin(), str);
            work_stack_copy.pop();
        }
        for (auto elem : elems)
            res += elem + " ";
        res.pop_back();
    }
    res += " }, { ";

    std::stack<std::string> input_stack_copy = input_stack;
    if (!input_stack_copy.empty())
    {
        while (!input_stack_copy.empty())
        {
            res += input_stack_copy.top();
            res += " ";
            input_stack_copy.pop();
        }
        res.pop_back();
    }
    res += " })";

    return res;
}

hrv::compiler::Parser & hrv::compiler::Parser::Expand()
{
    std::string top = _config.input_stack.top();
    _config.input_stack.pop();
    try
    {
        boost_set<ProductionExpression> productions = _grammar.GetProductionsOf(top);

        if (!productions.size()) throw std::runtime_error("Top of input stack has no productions in the given grammar");
        auto rhs = productions.begin()->rhs;

        for (auto it = rhs.rbegin(); it != rhs.rend(); ++it)
            _config.input_stack.push(*it);

        _config.work_stack.push(WorkStackItem(top, 0));
    }
    catch (std::runtime_error& e)
    {
        LOG(ERROR) << "Expansion of the configuration failed. Reason: " << e.what();
        _config.state = E;
    }

    return *this;
}

hrv::compiler::Parser & hrv::compiler::Parser::Advance()
{
    std::string top = _config.input_stack.top();
    _config.input_stack.pop();

    if (!_grammar.IsTerminal(top)) throw std::runtime_error("Advancing through the configuration during parsing failed. Reason: top of the input stack was not a terminal from the grammar");

    WorkStackItem item(top, WorkStackItem::INVALID_INDEX);
    _config.work_stack.push(item);
    ++_config.index;

    return *this;
}

hrv::compiler::Parser & hrv::compiler::Parser::MomentaryInsuccess()
{
    _config.state = B;

    return *this;
}

hrv::compiler::Parser & hrv::compiler::Parser::AnotherTry()
{
    if (_config.state != B) throw std::runtime_error("Another try during parsing failed. Reason: configuration state was not a back (B) state");

    WorkStackItem top = _config.work_stack.top();
    _config.work_stack.pop();
    auto productions = _grammar.GetProductionsOf(top.val);
    auto& last_production = productions[top.production_index];
    if (productions.size() == top.production_index + 1) // no more productions to try
    {
        for (size_t i = 0; i < last_production.rhs.size(); ++i)
        {
            if (_config.input_stack.top() != last_production.rhs[i]) throw std::runtime_error("Another try during parsing failed. Reason: the top elements of the input stack were not part of the last production recorded by the work stack");
            _config.input_stack.pop();
        }
        _config.input_stack.push(top.val);
        return *this;
    }
    else
    {
        for (size_t i = 0; i < last_production.rhs.size(); ++i)
        {
            if (_config.input_stack.top() != last_production.rhs[i]) throw std::runtime_error("Another try during parsing failed. Reason: the top elements of the input stack were not part of the last production recorded by the work stack");
            _config.input_stack.pop();
        }
        auto& next_production = productions[++top.production_index];
        for (auto it = next_production.rhs.rbegin(); it != next_production.rhs.rend(); ++it)
            _config.input_stack.push(*it);
        _config.work_stack.push(top);
        _config.state = Q;
    }
    return *this;
}

hrv::compiler::Parser & hrv::compiler::Parser::Back()
{
    if (_config.state != B) throw std::runtime_error("Back during parsing failed. Reason: configuration state was not a back (B) state");

    WorkStackItem top = _config.work_stack.top();
    _config.work_stack.pop();
    if (top.production_index == WorkStackItem::INVALID_INDEX)
    {
        _config.input_stack.push(top.val);
        --_config.index;
    }

    return *this;
}

hrv::compiler::Parser & hrv::compiler::Parser::Success()
{
    return *this;
}

std::string hrv::compiler::Parser::ConstructProductionString()
{
    auto stack_copy = _config.work_stack;

    std::string res;
    std::vector<std::string> tmp;
    while (!stack_copy.empty())
    {
        WorkStackItem item = stack_copy.top();
        stack_copy.pop();
        if (item.production_index != WorkStackItem::INVALID_INDEX)
            tmp.push_back(item.val + std::to_string(item.production_index));
    }
    std::reverse(tmp.begin(), tmp.end());
    for (std::string str : tmp)
        res += str + " ";
    if (res.size())
        res.pop_back(); // remove last space

    return res;
}

std::vector<std::string> hrv::compiler::Parser::ReconstructSequenceFromWorkStack()
{
    std::vector<std::string> res;
    std::stack<WorkStackItem> stack_copy = _config.work_stack;

    while (!stack_copy.empty())
    {
        auto top = stack_copy.top();
        stack_copy.pop();
        if (top.production_index == WorkStackItem::INVALID_INDEX)
            res.push_back(top.val);
    }

    std::reverse(res.begin(), res.end());
    return res;
}

std::string hrv::compiler::Parser::ParseInternal()
{
    size_t steps = 0;
    size_t input_size = _input_sequence.size();

    while (true)
    {
        LOG(INFO) << "Current configuration: " << _config.ToString();
        switch (_config.state)
        {
        case Q:
        {
            if (_config.input_stack.empty())
                _config.state = _input_sequence == ReconstructSequenceFromWorkStack() ? T : E;
            else
            {
                std::string top = _config.input_stack.top();
                if (_grammar.IsNonTerminal(top))
                {
                    LOG(INFO) << "Expanding.";
                    Expand();
                }
                else
                {
                    std::vector<std::string> work_stack_sequence = ReconstructSequenceFromWorkStack();
                    size_t next_elem_index = 0;
                    if (!work_stack_sequence.empty())
                        for (; next_elem_index < work_stack_sequence.size() && next_elem_index < _input_sequence.size() && work_stack_sequence[next_elem_index] == _input_sequence[next_elem_index]; ++next_elem_index)
                        {
                        }

                    if (next_elem_index < _input_sequence.size() && top == _input_sequence[next_elem_index])
                    {
                        LOG(INFO) << "Advancing.";
                        Advance();
                    }
                    else
                    {
                        LOG(INFO) << "Momentary insuccess.";
                        MomentaryInsuccess();
                    }
                }
            }
            break;
        }
        case B:
            if (_config.work_stack.top().production_index == WorkStackItem::INVALID_INDEX) // terminal at the top of the work stack
            {
                LOG(INFO) << "Back.";
                Back();
            }
            else // non-terminal at the top of the work stack
            {
                LOG(INFO) << "Another try.";
                AnotherTry();
            }
            break;
        case T:
            LOG(INFO) << "Finished.";
            return ConstructProductionString();
        case E:
            LOG(ERROR) << "Parsing finished with an error. ";
            return "Last configuration: " + _config.ToString();
        default:
            return "Unknown configuration state.";
        }
    }
}

hrv::compiler::Parser::Parser()
    : _config()
{}

hrv::compiler::Parser::Parser(const Grammar & g)
    : Parser()
{
    _grammar = g;
    _config.Init(_grammar);
}

std::string hrv::compiler::Parser::Parse(std::vector<std::string> input_sequence)
{
    if (input_sequence.empty())
        throw std::runtime_error("Cannot parse an empty input sequence");
    _input_sequence = input_sequence;

    return ParseInternal();
}

hrv::compiler::Parser & hrv::compiler::Parser::SetGrammar(const Grammar & g)
{
    _grammar = g;
    _config.Init(_grammar);

    return *this;
}

hrv::compiler::WorkStackItem::WorkStackItem(std::string val, size_t production_index)
    : val(val)
    , production_index(production_index)
{}
