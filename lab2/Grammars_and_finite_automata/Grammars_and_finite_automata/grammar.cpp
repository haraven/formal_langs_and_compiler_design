#include "grammar.h"
#include <iostream>
#include <algorithm>
#include <vector>
#include "utils.h"

bool hrv::compiler::Grammar::IsNonTermLHSTermRHS(const hrv::compiler::ProductionExpression & expr)
{
    return std::find_if
    (
        _non_terminals.begin(),
        _non_terminals.end(),
        [&expr](auto non_term)
        {
            return non_term.val == expr.lhs; // is LHS non-term
        }
    ) != _non_terminals.end() && std::find_if // and
    (
        _terminals.begin(),
        _terminals.end(),
        [&expr](auto term)
        {
            return expr.rhs.size() == 1 && term == expr.rhs[0]; // is RHS term
        }
    ) != _terminals.end();
}

bool hrv::compiler::Grammar::RightIsNonTermLHSCompositeRHS(const hrv::compiler::ProductionExpression & expr)
{
    bool cond =  std::find_if
    (
        _non_terminals.begin(),
        _non_terminals.end(),
        [&expr](auto non_term)
        {
            return non_term.val == expr.lhs; // is LHS non-term
        }
    ) != _non_terminals.end();
    if (expr.rhs.size() == 2)
    {
        bool tmp = std::find_if
        (
            _terminals.begin(),
            _terminals.end(),
            [&expr](auto term) // is first element of RHS term
            {
                return term == expr.rhs[0];
            }
        ) != _terminals.end() && std::find_if
        (
            _non_terminals.begin(),
            _non_terminals.end(),
            [&expr](auto non_term) // is second element of RHS non-term
            {
                return non_term.val == expr.rhs[1];
            }
        ) != _non_terminals.end();

        cond = cond && tmp;
    }
    else 
        cond = false;

    return cond;
}

bool hrv::compiler::Grammar::LeftIsNonTermLHSCompositeRHS(const hrv::compiler::ProductionExpression & expr)
{
    bool cond =  std::find_if
    (
        _non_terminals.begin(),
        _non_terminals.end(),
        [&expr](auto non_term)
        {
            return non_term.val == expr.lhs; // is LHS non-term
        }
    ) != _non_terminals.end();
    if (expr.rhs.size() == 2)
    {
        bool tmp = std::find_if
        (
            _terminals.begin(),
            _terminals.end(),
            [&expr](auto term) // is second element of RHS term
            {
                return term == expr.rhs[1];
            }
        ) != _terminals.end() && std::find_if
        (
            _non_terminals.begin(),
            _non_terminals.end(),
            [&expr](auto non_term) // is first element of RHS non-term
            {
                return non_term.val == expr.rhs[0];
            }
        ) != _non_terminals.end();

        cond = cond && tmp;
    }
    else 
        cond = false;

    return cond;
}

bool hrv::compiler::Grammar::StartingSymbolHasEpsilonProductionAndIsOnRHS() const
{
    Symbol starting_symbol = GetStartingSymbol();
    bool is_epsilon = false;
    for (auto expr : _productions)
        if (expr.lhs == starting_symbol.val && expr.IsEpsilon())
        {
            is_epsilon = true;
            break;
        }

    if (is_epsilon)
        for (auto production : _productions)
            if (!production.IsEpsilon() && production.RHSContains(starting_symbol.val))
                return true;

    return false;
}

std::string hrv::compiler::Grammar::RandomSymbol() const
{
    size_t name_size = 1;
    std::string name;
    bool found = false;
    do
    {
        name = utils::RandomStr(name_size);
        std::for_each(_non_terminals.cbegin(), _non_terminals.cend(), [&name_size, &name, &found](auto non_term)
        {
            if (non_term.val == name)
            {
                found = true;
                ++name_size;
            }
        });
        std::for_each(_terminals.cbegin(), _terminals.cend(), [&name_size, &name, &found](auto term)
        {
            if (term == name)
            {
                found = true;
                ++name_size;
            }
        });
    } while (found);

    return name;
}

bool hrv::compiler::Grammar::ContainsOnlyTerms(const ProductionExpression & expr) const
{
    const std::vector<std::string>& rhs = expr.rhs;
    for (auto elem : rhs)
    {
        bool is_term = false;
        for (auto term : _terminals)
            if (term == elem)
            {
                is_term = true;
                break;
            }
        if (!is_term)
            return false;
    }

    return true;
}

bool hrv::compiler::Grammar::StartingSymbolHasEpsilonProduction() const
{
    const Symbol& starting_symbol = GetStartingSymbol();
    return std::any_of(_productions.cbegin(), _productions.cend(), [this, &starting_symbol](auto expr)
    {
        return expr.lhs == starting_symbol.val && (expr.IsEpsilon());
    });
}

hrv::compiler::Grammar::Grammar()
{}

hrv::compiler::Grammar & hrv::compiler::Grammar::AddNonTerm(const Symbol & nonterm)
{
    _non_terminals.insert(nonterm);

    return *this;
}

hrv::compiler::Grammar & hrv::compiler::Grammar::AddNonTerms(const std::set<Symbol>& nonterms)
{
    _non_terminals.insert(nonterms.begin(), nonterms.end());

    return *this;
}

hrv::compiler::Grammar & hrv::compiler::Grammar::AddTerm(const std::string term)
{
    _terminals.insert(term);

    return *this;
}

hrv::compiler::Grammar & hrv::compiler::Grammar::AddTerms(const std::set<std::string>& terms)
{
    _terminals.insert(terms.begin(), terms.end());

    return *this;
}

hrv::compiler::Grammar & hrv::compiler::Grammar::AddProduction(const ProductionExpression & expr)
{
    _productions.push_back(expr);

    return *this;
}

hrv::compiler::Grammar & hrv::compiler::Grammar::AddProductions(const boost_set<ProductionExpression>& expressions)
{
    _productions.insert(_productions.end(), expressions.begin(), expressions.end());

    return *this;
}

hrv::compiler::Grammar & hrv::compiler::Grammar::Clear()
{
    _non_terminals.clear();
    _terminals.clear();
    _productions.clear();

    return *this;
}

bool hrv::compiler::Grammar::ContainsStartingSymbol() const
{
    return std::any_of(_non_terminals.begin(), _non_terminals.end(), [](auto non_term)
    {
        return non_term.is_starting_symbol;
    });
}

bool hrv::compiler::Grammar::IsNonTerminal(const std::string & symbol) const
{
    return std::any_of(_non_terminals.cbegin(), _non_terminals.cend(), [&symbol](auto nonterm)
    {
        return nonterm.val == symbol;
    });
}

bool hrv::compiler::Grammar::IsTerminal(const std::string & symbol) const
{
    return std::any_of(_terminals.cbegin(), _terminals.cend(), [&symbol](auto term)
    {
        return term == symbol;
    });
}

const hrv::compiler::Symbol& hrv::compiler::Grammar::GetStartingSymbol() const
{
    auto it = std::find_if(_non_terminals.cbegin(), _non_terminals.cend(), [](auto non_term)
    {
        return non_term.is_starting_symbol;
    });

    if (it == _non_terminals.cend())
        throw std::runtime_error("No starting symbol found");
    else return *it;
}


hrv::compiler::boost_set<hrv::compiler::ProductionExpression> hrv::compiler::Grammar::GetProductionsOf(const std::string & non_term) const
{
    if (!IsNonTerminal(non_term))
        throw std::runtime_error("Could not retrieve productions of \"" + non_term + "\" because \"" + non_term + "\" is not a non-terminal");
    boost_set<ProductionExpression> expressions;

    auto it = std::find_if(_non_terminals.begin(), _non_terminals.end(), [&non_term](auto nt)
    {
        return non_term == nt.val;
    });

    if (it != _non_terminals.end())
    {
        Symbol symbol = *it;
        std::for_each(_productions.begin(), _productions.end(), [&expressions, &symbol](auto expr)
        {
            if (expr.lhs == symbol.val)
                expressions.push_back(expr);
        });

        return expressions;
    }
    else throw std::runtime_error("Could not find non-terminal \"" + non_term + "\"");
}

hrv::compiler::Grammar & hrv::compiler::Grammar::operator=(const hrv::compiler::Grammar & ot)
{
    _non_terminals = ot._non_terminals;
    _terminals     = ot._terminals;
    _productions   = ot._productions;

    return *this;
}

bool hrv::compiler::Grammar::IsRightRegular()
{

    for (auto it = _productions.begin(); it != _productions.end(); ++it)
    {
        bool b1 = IsNonTermLHSTermRHS(*it);
        bool b2 = RightIsNonTermLHSCompositeRHS(*it);
        bool b3 = it->IsEpsilon() && GetStartingSymbol().val == it->lhs;
        if (!(b1 || b2 || b3))
            return false;
    }
    return true;
}

bool hrv::compiler::Grammar::IsLeftRegular()
{
    for (auto it = _productions.begin(); it != _productions.end(); ++it)
    {
        bool b1 = IsNonTermLHSTermRHS(*it);
        bool b2 = LeftIsNonTermLHSCompositeRHS(*it);
        bool b3 = it->IsEpsilon() && GetStartingSymbol().val == it->lhs;
        if (!(b1 || b2 || b3))
            return false;
    }

    return true;
}

bool hrv::compiler::Grammar::IsRegular()
{
    bool right = IsRightRegular();
    bool left = IsLeftRegular();
    bool epsilon_start_on_rhs = StartingSymbolHasEpsilonProductionAndIsOnRHS();

    return !epsilon_start_on_rhs && (left || right);
}

bool hrv::compiler::Grammar::IsContextFree()
{
    return std::all_of(_productions.cbegin(), _productions.cend(), [this](auto production)
    {
        if (!IsNonTerminal(production.lhs)) // lhs not nonterminal
            return false;

        if (!production.IsEpsilon()) // if not epsilon production
            for (size_t i = 0; i < production.rhs.size(); ++i) // get each symbol on the right hand side, and check if it's a terminal or non-terminal from the vocabulary of the grammar
                if (!(IsNonTerminal(production.rhs[i]) || IsTerminal(production.rhs[i])))
                    return false;

        return true;
    });
}

std::string hrv::compiler::Grammar::ToString() const
{
    std::string res = "Non-terminals: [ ";

    std::for_each(_non_terminals.begin(), _non_terminals.end(), [&res](auto symbol)
    {
        res += symbol.ToString();
        res += ", ";
    });

    if (res.substr(res.size() - 2) == ", ")
    {
        res.pop_back();
        res.pop_back();
    }
    res += " ]; Terminals: [ ";

    std::for_each(_terminals.begin(), _terminals.end(), [&res](auto term)
    {
        res += term;
        res += ", ";
    });

    if (res.substr(res.size() - 2) == ", ")
    {
        res.pop_back();
        res.pop_back();
    }
    res += " ]; Productions: [ ";

    std::for_each(_productions.begin(), _productions.end(), [&res](auto production)
    {
        res += production.ToString();
        res += ", ";
    });
    if (res.substr(res.size() - 2) == ", ")
    {
        res.pop_back();
        res.pop_back();
    }
    res += " ]";

    return res;
}

hrv::compiler::Grammar::~Grammar()
{}

std::string hrv::compiler::GrammarReader::SimplifyLine(std::string line)
{
    std::string::iterator new_end = std::unique(line.begin(), line.end(), [](char lhs, char rhs) { return (lhs == rhs) && (lhs == ' '); });
    line.erase(new_end, line.end());

    return line;
}

std::vector<std::vector<std::string>> hrv::compiler::GrammarReader::ParseProductionRHS(const std::string & rhs)
{
    std::vector<std::vector<std::string>> res;
    std::vector<std::string> split_rhs = utils::SplitNoNeighboringDelims(rhs, '|'); // need to use special Split function in case the user inputs productions that contain || or something similar as terminals
    res.reserve(split_rhs.size());
    for (auto single_rhs : split_rhs)
        res.push_back(utils::Split(single_rhs, ' '));

    return res;
}

void hrv::compiler::GrammarReader::HandleLines(Iterator & line_it, Grammar& g)
{
    if (line_it == End())
        return;

    while ((*line_it).substr(0, 2) == "//") // skip comments
        ++line_it;

    std::string line = *line_it;
    if (line == _non_term_indicator)
    {
        ++line_it;
        if (line_it != End())
            line = *line_it;
        else return;

        while (line != _term_indicator && line != _production_indicator && line != _non_term_indicator && line_it != End())
        {
            if (line.substr(0, 2) != "//")
                HandleNonTerminal(line, g);
            ++line_it;
            if (line_it != End())
                line = *line_it;
            else return;
        }
    }
    
    if (line == _term_indicator)
    {
        ++line_it;
        if (line_it != End())
            line = *line_it;
        else return;

        while (line != _term_indicator && line != _production_indicator && line != _non_term_indicator && line_it != End())
        {
            if (line.substr(0, 2) != "//")
                HandleTerminal(line, g);
            ++line_it;
            if (line_it != End())
                line = *line_it;
            else return;
        }
    }

    if (line == _production_indicator)
    {
        ++line_it;
        if (line_it != End())
            line = *line_it;
        else return;

        while (line != _term_indicator && line != _production_indicator && line != _non_term_indicator && line_it != End())
        {
            if (line.substr(0, 2) != "//")
                HandleProduction(line, g);
            ++line_it;
            if (line_it != End())
                line = *line_it;
            else return;
        }
    }
}

void hrv::compiler::GrammarReader::HandleNonTerminal(const std::string & line, Grammar & g)
{
    g.AddNonTerm(HandleNonTerminal(line));
}

void hrv::compiler::GrammarReader::HandleTerminal(const std::string & line, Grammar & g)
{
    g.AddTerms(HandleTerminals(line));
}

void hrv::compiler::GrammarReader::HandleProduction(const std::string & line, Grammar & g)
{
    g.AddProductions(HandleProductions(line));
}

hrv::compiler::Symbol hrv::compiler::GrammarReader::HandleNonTerminal(const std::string & line)
{
    Symbol s;
    if (line == "" || line == _finished_indicator || line == _term_indicator || line == _non_term_indicator || line == _production_indicator)
        return s;

    std::string trimmed_line = utils::TrimCharacter(line, ' ');
    std::vector<std::string> split_line = utils::Split(trimmed_line, ',');
    if (split_line.size() < 2)
        return s;
    if (split_line.size() > 2) throw std::runtime_error("Error reading non-terminal: \"" + line + "\" doesn't contain a valid non-terminal");
    s.is_starting_symbol = split_line[1] == STRING_TRUE ? true : false;
    s.val = split_line[0];

    return s;
}

std::set<std::string> hrv::compiler::GrammarReader::HandleTerminals(const std::string & line)
{
    std::set<std::string> terminals;
    if (line == "" || line == _finished_indicator || line == _term_indicator || line == _non_term_indicator || line == _production_indicator)
        return terminals;

    std::string trimmed_line = utils::TrimCharacter(line, ' ');
    std::vector<std::string> split_line = utils::Split(trimmed_line, ',');
    if (split_line.size() == 0)
        terminals.insert(trimmed_line);
    else for (auto token : split_line)
        terminals.insert(token);

    return terminals;
}

hrv::compiler::boost_set<hrv::compiler::ProductionExpression> hrv::compiler::GrammarReader::HandleProductions(const std::string & line)
{
    boost_set<hrv::compiler::ProductionExpression> productions;
    if (line == "" || line == _finished_indicator || line == _term_indicator || line == _non_term_indicator || line == _production_indicator)
        return productions;

    std::string simplified_line = SimplifyLine(line);
    ProductionExpression expr;
    std::vector<std::string> split_line = utils::Split(simplified_line, '>');
    if (split_line.size() != 2)
        throw std::runtime_error("Error parsing grammar file \"" + _filename + "\": line \"" + line + "\" doesn't contain a valid production");
    expr.operation = OP_IMPLIES;
    expr.lhs = utils::TrimCharacter(split_line[0], ' ');
    std::vector<std::vector<std::string>> rhs = ParseProductionRHS(split_line[1]);
    for (auto single_rhs : rhs)
    {
        expr.rhs = single_rhs;
        productions.push_back(expr);
    }

    return productions;
}

hrv::compiler::GrammarReader::GrammarReader(const std::string & filename)
: FileReader(filename, SEPARATOR_NEWLINE)
, _non_term_indicator("# NONTERMS")
, _term_indicator("# TERMS")
, _production_indicator("# PRODUCTIONS")
, _finished_indicator("# FINISHED")
{}

hrv::compiler::Grammar hrv::compiler::GrammarReader::GetGrammar()
{
    if (_filename == NO_FILE)
        throw std::runtime_error("Could not retrieve grammar from file. Reason: no file was opened");

    Grammar g;

    Iterator line_it = Begin();
    HandleLines(line_it, g);

    return g;
}

std::set<hrv::compiler::Symbol> hrv::compiler::GrammarReader::GetNonTerminalsFromStream(std::istream & in)
{
    std::set<Symbol> non_terms;
    std::string line;
    do
    {
        std::getline(in, line);
        if (line == _finished_indicator || line == _term_indicator || line == _non_term_indicator || line == _production_indicator)
            break;
        if (line == "")
            continue;
        non_terms.insert(HandleNonTerminal(line));
    } while (line != _finished_indicator & line != _term_indicator && line != _production_indicator);

    return non_terms;
}

std::set<std::string> hrv::compiler::GrammarReader::GetTerminalsFromStream(std::istream & in)
{
    std::set<std::string> non_terms;
    std::string line;
    do
    {
        std::getline(in, line);
        if (line == _finished_indicator || line == _term_indicator || line == _non_term_indicator || line == _production_indicator)
            break;
        if (line == "")
            continue;
        std::set<std::string> tmp = HandleTerminals(line);
        non_terms.insert(tmp.begin(), tmp.end());
    } while (line != _finished_indicator & line != _non_term_indicator && line != _production_indicator);

    return non_terms;
}

hrv::compiler::boost_set<hrv::compiler::ProductionExpression> hrv::compiler::GrammarReader::GetProductionsFromStream(std::istream & in)
{
    boost_set<ProductionExpression> productions;
    std::string line;
    do
    {
        std::getline(in, line);
        if (line == _finished_indicator || line == _term_indicator || line == _non_term_indicator || line == _production_indicator)
            break;
        if (line == "")
            continue;
        boost_set<ProductionExpression> tmp = HandleProductions(line);
        productions.insert(productions.end(), tmp.begin(), tmp.end());
    } while (line != _finished_indicator & line != _non_term_indicator && line != _term_indicator);

    return productions;
}

bool hrv::compiler::operator<(const hrv::compiler::Symbol & left, const hrv::compiler::Symbol & right)
{
    return left.val < right.val;
}

bool hrv::compiler::operator<(const hrv::compiler::ProductionExpression & l, const hrv::compiler::ProductionExpression & r)
{
    return l.lhs + utils::VectorToString(l.rhs) < r.lhs + utils::VectorToString(r.rhs);
}

bool hrv::compiler::ProductionExpression::operator==(const hrv::compiler::ProductionExpression & ot)
{
    return lhs == ot.lhs && operation == ot.operation && rhs == ot.rhs;
}

bool hrv::compiler::ProductionExpression::IsEpsilon() const
{
    return rhs.size() == 1 && rhs[0] == EPSILON;
}

bool hrv::compiler::ProductionExpression::RHSContains(std::string elem) const
{
    return std::find(rhs.cbegin(), rhs.cend(), elem) != rhs.cend();
}

std::string hrv::compiler::ProductionExpression::ToString() const
{
    return lhs + (operation == OP_IMPLIES ? " -> " : operation == OP_OR ? "|" : "�") + utils::VectorToString(rhs);
}

std::string hrv::compiler::Symbol::ToString() const
{
    return val + (is_starting_symbol ? ": start" : "");
}

hrv::compiler::Grammar hrv::compiler::GrammarHelper::EliminateInaccesibleSymbols(const hrv::compiler::Grammar & g)
{
    Grammar res;

    std::set<std::string> prev; // vi-1

    std::set<std::string> next; // vi
    next.insert(g.GetStartingSymbol().val);
    do
    {
        prev = next;
        for (std::string symbol : prev) // gotta love that fantastic O(n * m * l) complexity
        {
            for (auto& production : g._productions) 
                if (production.lhs == symbol)
                {
                    if (!production.IsEpsilon())
                        for (size_t i = 0; i < production.rhs.size(); ++i)
                            next.insert(production.rhs[i]);
                }
        }
    } while (!(prev == next));

    // nonterms
    for (auto nonterm : g._non_terminals)
        for (auto symbol : next)
            if (nonterm.val == symbol)
                res.AddNonTerm(nonterm);

    // terms
    for (auto term : g._terminals)
        for (auto symbol : next)
            if (term == symbol)
                res.AddTerm(term);
    
    // productions
    for (auto production : g._productions)
    {
        if (production.IsEpsilon())
            res.AddProduction(production);
        else
        {
            bool is_lhs_in_n_prime = std::any_of(res._non_terminals.begin(), res._non_terminals.end(), [&production](auto nonterm)
            {
                return nonterm.val == production.lhs;
            });
            bool is_rhs_in_nue = std::all_of(production.rhs.begin(), production.rhs.end(), [&next](std::string elem)
            {
                return next.find(elem) != next.end();
            });

            if (is_lhs_in_n_prime && is_rhs_in_nue)
                res.AddProduction(production);
        }
    }

    return res;
}
