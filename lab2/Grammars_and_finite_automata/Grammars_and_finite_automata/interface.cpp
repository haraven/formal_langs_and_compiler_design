#include "interface.h"
#include <algorithm>
#include "utils.h"
#include "my_converter.h"

hrv::ui::MenuItemBuilder::MenuItemBuilder()
    : _name(INVALID_MENU_ITEM_NAME)
    , _prefix(MENU_ITEM_DEFAULT_PREFIX)
{}

hrv::ui::MenuItemBuilder & hrv::ui::MenuItemBuilder::Name(const std::string & name)
{
    _name = name;

    return *this;
}

hrv::ui::MenuItemBuilder & hrv::ui::MenuItemBuilder::Prefix(const std::string & prefix)
{
    _prefix = prefix;

    return *this;
}

hrv::ui::MenuItem * hrv::ui::MenuItemBuilder::Build()
{
    return new MenuItem(_name, _prefix);
}

hrv::ui::MenuItem::MenuItem(const std::string & name, const std::string& prefix)
    : _name(name)
    , _prefix(prefix)
{}

hrv::ui::MenuItemBuilder & hrv::ui::MenuItem::Builder()
{
    static MenuItemBuilder builder;

    return builder;
}

hrv::ui::MenuItem & hrv::ui::MenuItem::Parse(std::ostream & out)
{
    out << _prefix << ' ' << _name;

    return *this;
}

hrv::ui::Menu::Menu()
{}

hrv::ui::Menu & hrv::ui::Menu::Parse(std::ostream & out)
{
    out << "---------------------------------------------------------------" << std::endl;
    std::for_each(_menu_items.begin(), _menu_items.end(), [&out](auto item)
    {
        item->Parse(out);
        out << std::endl;
    });
    out << "---------------------------------------------------------------" << std::endl;

    return *this;
}

hrv::ui::Menu & hrv::ui::Menu::Add(UIElement* item)
{
    _menu_items.push_back(item);

    return *this;
}

hrv::ui::Menu::~Menu()
{
    std::for_each(_menu_items.begin(), _menu_items.end(), [](auto item)
    {
        utils::SafeDelete(item);
    });

    _menu_items.clear();
}

void hrv::ui::Interface::HandleExceptionPtr(std::exception_ptr eptr)
{
    try 
    {
        if (eptr) 
            std::rethrow_exception(eptr);
    }
    catch (const std::exception& e) 
    {
        _out << "Unexpected error occurred: " << e.what() << std::endl;
    }
}

void hrv::ui::Interface::InitMainMenu()
{
    MenuItemBuilder& builder = MenuItem::Builder();
    if (_main_menu)
        _main_menu->Add(builder.Name("Read grammar (rga)").Build())
        .Add(builder.Name("Display current grammar (dga)").Build())
        .Add(builder.Name("Check if grammar is regular (isgreg)").Build())
        .Add(builder.Name("Check if grammar is context-free (iscfg)").Build())
        .Add(builder.Name("Eliminate inaccessible symbols from grammar (eis)").Build())
        .Add(builder.Name("Read finite automaton (rfa)").Build())
        .Add(builder.Name("Display finite automaton (dfa)").Build())
        .Add(builder.Name("Grammar to finite automaton (g2fa)").Build())
        .Add(builder.Name("Finite automaton to grammar (fa2g)").Build())
        .Add(builder.Name("Parse input sequence(pin)").Build())
        .Add(builder.Name("Show main menu (help)").Build())
        .Add(builder.Name("Exit (exit)").Build());
}

void hrv::ui::Interface::InitReadMenu()
{
    MenuItemBuilder& builder = MenuItem::Builder();
    if (_read_menu)
        _read_menu->Add(builder.Name("Read from file (file)").Build())
        .Add(builder.Name("Read from standard input (stdin)").Build())
        .Add(builder.Name("Cancel (cancel)").Build());
}

void hrv::ui::Interface::InitDisplayGrammarMenu()
{
    MenuItemBuilder& builder = MenuItem::Builder();
    if (_display_grammar_menu)
        _display_grammar_menu->Add(builder.Name("Set of terminals (term)").Build())
        .Add(builder.Name("Set of non-terminals (nterm)").Build())
        .Add(builder.Name("Set of productions (p)").Build())
        .Add(builder.Name("Productions of non-terminal symbol (pnterm)").Build())
        .Add(builder.Name("Cancel (cancel)").Build());
}

void hrv::ui::Interface::InitDisplayFAMenu()
{
    MenuItemBuilder& builder = MenuItem::Builder();
    if (_display_fa_menu)
        _display_fa_menu->Add(builder.Name("Set of states (s)").Build())
        .Add(builder.Name("The alphabet (alpha)").Build())
        .Add(builder.Name("Transitions (trans)").Build())
        .Add(builder.Name("Set of final states (fs)").Build())
        .Add(builder.Name("Cancel (cancel)").Build());
}

hrv::ui::Interface::Command hrv::ui::Interface::InputToCommand(const std::string & input)
{
    if (input == "exit")
        return CMD_EXIT;
    if (input == "help")
        return CMD_HELP;
    if (input == "rga")
        return CMD_READ_GRAMMAR;
    if (input == "dga")
        return CMD_DISPLY_GRAMMAR;
    if (input == "isgreg")
        return CMD_IS_GRAMMAR_REGULAR;
    if (input == "iscfg")
        return CMD_IS_GRAMMAR_CF;
    if (input == "eis")
        return CMD_ELIMINATE_INACCESSIBLE_SYMBOLS;
    if (input == "rfa")
        return CMD_READ_FA;
    if (input == "dfa")
        return CMD_DISPLAY_FA;
    if (input == "g2fa")
        return CMD_GRAMMAR_TO_FA;
    if (input == "fa2g")
        return CMD_FA_TO_GRAMMAR;
    if (input == "file")
        return CMD_FILE;
    if (input == "stdin")
        return CMD_STDIN;
    if (input == "term")
        return CMD_TERMINALS;
    if (input == "nterm")
        return CMD_NON_TERMINALS;
    if (input == "p")
        return CMD_PRODUCTIONS;
    if (input == "pnterm")
        return CMD_PRODUCTIONS_OF_NON_TERMINAL;
    if (input == "s")
        return CMD_STATES;
    if (input == "alpha")
        return CMD_ALPHABET;
    if (input == "trans")
        return CMD_TRANSITIONS;
    if (input == "fs")
        return CMD_FINAL_STATES;
    if (input == "cancel")
        return CMD_CANCEL;
    if (input == "pin")
        return CMD_PARSE_INPUT;

    return CMD_UNKNOWN;
}

hrv::ui::Interface::Interface(std::ostream & out, std::istream& in)
: _out(out)
, _in(in)
, _main_menu(nullptr)
, _read_menu(nullptr)
, _display_fa_menu(nullptr)
, _display_grammar_menu(nullptr)
, _parser()
{}

hrv::ui::Interface & hrv::ui::Interface::Init()
{
    if (!_main_menu)
    {
        _main_menu = new Menu();
        InitMainMenu();
    }
    if (!_read_menu)
    {
        _read_menu = new Menu();
        InitReadMenu();
    }
    if (!_display_grammar_menu)
    {
        _display_grammar_menu = new Menu();
        InitDisplayGrammarMenu();
    }
    if (!_display_fa_menu)
    {
        _display_fa_menu = new Menu();
        InitDisplayFAMenu();
    }

    return *this;
}

hrv::ui::Interface & hrv::ui::Interface::Show()
{
    _out << "Welcome. Please choose your action (type the name of the command contained within parantheses):" << std::endl;
    _main_menu->Parse(_out);
    std::string input;
    std::string intermediate_input;
    std::string filename;

    hrv::compiler::GrammarReader grammar_reader;
    std::set<compiler::Symbol> non_terminals;
    std::set<std::string> terminals;
    compiler::boost_set<compiler::ProductionExpression> productions;

    hrv::compiler::AutomatonReader automaton_reader;
    std::set<compiler::State> states;
    std::set<std::string> alphabet;
    compiler::Automaton::TransitionFnMap transition_fn;

    while (true)
    {
        try
        {
            _out << "> ";
            _in >> input;
            Command command = InputToCommand(input);
            switch (command)
            {
            case CMD_EXIT:
                _out << "Exiting application..." << std::endl;
                exit(EXIT_SUCCESS);
            case CMD_HELP:
                _main_menu->Parse(_out);
                break;
            case CMD_READ_GRAMMAR:
            {
                _read_menu->Parse(_out);
                _out << "> ";
                _in >> intermediate_input;
                Command read_grammar_command = InputToCommand(intermediate_input);
                switch (read_grammar_command)
                {
                case CMD_FILE:
                    _out << "Enter a filename\n> ";

                    _in >> filename;
                    grammar_reader.Open(filename, "\n");
                    _grammar = grammar_reader.GetGrammar();
                    grammar_reader.Close();

                    _out << "Done" << std::endl;
                    break;
                case CMD_STDIN:
                    _out << "Enter the list of non-terminals (a non-terminal must be on a new line, and must be of the form (symbol, is_starting_symbol), where is_starting_symbol is a boolean (true or false)). Type \"# TERMS\" to start inputting terminals\n> ";
                    non_terminals = grammar_reader.GetNonTerminalsFromStream(_in);

                    _out << "Enter the list of terminals (terminals can entered in the form of term1, term2, term3, ..., termn, where termi is a terminal symbol, 1 <= i <= n, or each terminal can be on its own line). Type \"# PRODUCTIONS\" to start inputting productions\n> ";
                    terminals = grammar_reader.GetTerminalsFromStream(_in);

                    _out << "Enter the list of productions (productions can be entered in the form prod1, prod2, prod3, ..., prodn, where prodi is a production, 1 <= i <=n, or each production can b on its own line; a production is of the form nonterm > terms [ | terms]). Type \"# FINISHED\" to create the grammar\n> ";
                    productions = grammar_reader.GetProductionsFromStream(_in);

                    std::for_each(non_terminals.begin(), non_terminals.end(), [this](auto non_term)
                    {
                        _grammar.AddNonTerm(non_term);
                    });
                    std::for_each(terminals.begin(), terminals.end(), [this](auto term)
                    {
                        _grammar.AddTerm(term);
                    });
                    std::for_each(productions.begin(), productions.end(), [this](auto production)
                    {
                        _grammar.AddProduction(production);
                    });

                    _out << "Done" << std::endl;
                    break;
                case CMD_CANCEL: break;
                default:
                    _out << "Unknown command" << std::endl;
                    break;
                }
                break;
            }
            case CMD_DISPLY_GRAMMAR:
            {
                _display_grammar_menu->Parse(_out);
                _out << "> ";
                _in >> intermediate_input;
                Command display_grammar_command = InputToCommand(intermediate_input);
                switch (display_grammar_command)
                {
                case CMD_TERMINALS:
                    _out << "Terminals: [ ";
                    std::for_each(_grammar.GetTerminals().cbegin(), _grammar.GetTerminals().cend(), [this](auto term)
                    {
                        _out << term << " ";
                    });
                    _out << "]" << std::endl;
                    break;
                case CMD_NON_TERMINALS:
                    _out << "Non-terminals: [ ";
                    std::for_each(_grammar.GetNonTerminals().cbegin(), _grammar.GetNonTerminals().cend(), [this](auto non_term)
                    {
                        _out << non_term.ToString() << " ";
                    });
                    _out << "]" << std::endl;
                    break;
                case CMD_PRODUCTIONS:
                    _out << "Productions: [ ";
                    std::for_each(_grammar.GetProductions().cbegin(), _grammar.GetProductions().cend(), [this](auto production)
                    {
                        _out << production.ToString() << " ";
                    });
                    _out << "]" << std::endl;
                    break;
                case CMD_PRODUCTIONS_OF_NON_TERMINAL:
                    _out << "Enter the non-terminal to display the productions of\n> ";
                    _in >> intermediate_input;
                    productions = _grammar.GetProductionsOf(intermediate_input);
                    _out << "Productions of " + intermediate_input + ": [ ";
                    std::for_each(productions.cbegin(), productions.cend(), [this](auto production)
                    {
                        _out << production.ToString() << " ";
                    });
                    _out << "]" << std::endl;
                    break;
                case CMD_CANCEL:
                    break;
                default:
                    _out << "Unknown command" << std::endl;
                    break;
                }
                break;
            }
            case CMD_IS_GRAMMAR_REGULAR:
                _out << "Grammar is " << (_grammar.IsRegular() ? "regular" : "not regular") << std::endl;
                break;
            case CMD_IS_GRAMMAR_CF:
                _out << "Grammar is " << (_grammar.IsContextFree() ? "context-free" : "not context-free") << std::endl;
                break;
            case CMD_ELIMINATE_INACCESSIBLE_SYMBOLS:
                _grammar = compiler::GrammarHelper::EliminateInaccesibleSymbols(_grammar);
                _out << "Done" << std::endl;
                break;
            case CMD_READ_FA:
            {
                _read_menu->Parse(_out);
                _out << "> ";
                _in >> intermediate_input;
                Command read_fa_command = InputToCommand(intermediate_input);

                switch (read_fa_command)
                {
                case CMD_FILE:
                    _out << "Enter a filename\n> ";

                    _in >> filename;
                    automaton_reader.Open(filename, "\n");
                    _finite_automaton = automaton_reader.GetAutomaton();
                    automaton_reader.Close();

                    _out << "Done" << std::endl;
                    break;
                case CMD_STDIN:
                    _out << "Enter the list of states (a state must be on a new line, and must be of the form (symbol, is_initial_state, is_final_state), where is_initial_state and is_final_state are booleans (true or false)). Type \"# ALPHABET\" to start inputting the alphabet\n> ";
                    states = automaton_reader.GetStatesFromStream(_in);

                    _out << "Enter the alphabet (alphabet symbols can entered in the form of term1, term2, term3, ..., termn, where termi is a symbol from the alphabet, 1 <= i <= n, or each alphabet symbol can be on its own line). Type \"# TRANSITION\" to start inputting the values of the transition function\n> ";
                    alphabet = automaton_reader.GetAlphabetFromStream(_in);

                    std::for_each(states.begin(), states.end(), [this](auto state)
                    {
                        _finite_automaton.AddState(state);
                    });
                    std::for_each(alphabet.begin(), alphabet.end(), [this](auto symbol)
                    {
                        _finite_automaton.AddSymbolToAlphabet(symbol);
                    });

                    _out << "Enter the transition function values (each value must be on a new line, and must be of the form (state, alphabet_symbol : state1, state2, state3, ..., staten, where statei is a pre-existing state, 1 <= i <= n). Type \"# FINISHED\" to create the automaton\n> ";
                    automaton_reader.GetTransitionFnFromStream(_in, _finite_automaton);

                    _out << "Done" << std::endl;
                    break;
                case CMD_CANCEL:
                    break;
                default:
                    _out << "Unknown command" << std::endl;
                    break;
                }
                break;
            }
            case CMD_DISPLAY_FA:
            {
                _display_fa_menu->Parse(_out);
                _out << "> ";
                _in >> intermediate_input;
                Command display_fa_command = InputToCommand(intermediate_input);
                switch (display_fa_command)
                {
                case CMD_STATES:
                    states = _finite_automaton.GetStates();
                    _out << "States: [ ";
                    std::for_each(states.cbegin(), states.cend(), [this](auto state)
                    {
                        _out << state.ToString() << " ";
                    });
                    _out << "]" << std::endl;
                    break;
                case CMD_ALPHABET:
                    alphabet = _finite_automaton.GetAlphabet();
                    _out << "Alphabet: [ ";
                    std::for_each(alphabet.cbegin(), alphabet.cend(), [this](auto symbol)
                    {
                        _out << symbol << " ";
                    });
                    _out << "]" << std::endl;
                    break;
                case CMD_TRANSITIONS:
                    transition_fn = _finite_automaton.GetTransitionFn();
                    _out << "Transition function delta(Q, sigma): [ ";
                    std::for_each(transition_fn.cbegin(), transition_fn.cend(), [this](auto kvp)
                    {
                        _out << std::endl << "(" << kvp.first.first.val << ", " << kvp.first.second << ") = ";
                        std::for_each(kvp.second.cbegin(), kvp.second.cend(), [this](auto state)
                        {
                            _out << state.ToString() << " ";
                        });
                    });
                    _out << "]" << std::endl;
                    break;
                case CMD_FINAL_STATES:
                    states = _finite_automaton.GetFinalStates();
                    _out << "Final states: [ ";
                    std::for_each(states.cbegin(), states.cend(), [this](auto state)
                    {
                        _out << state.val << " ";
                    });
                    _out << "]" << std::endl;
                    break;
                case CMD_CANCEL:
                    break;
                default:
                    _out << "Unknown command" << std::endl;
                    break;
                }
                break;
            }
            case CMD_GRAMMAR_TO_FA:
                _finite_automaton = compiler::MyConverter::RegularGrammarToAutomaton(_grammar);
                _out << "Done" << std::endl;
                break;
            case CMD_FA_TO_GRAMMAR:
                _grammar = compiler::MyConverter::FiniteAutomatonToRegularGrammar(_finite_automaton);
                _out << "Done" << std::endl;
                break;
            default:
                _out << "Unknown command" << std::endl;
                break;
            case CMD_PARSE_INPUT:
                _parser.SetGrammar(_grammar);
                _out << "Enter the input sequence to parse; separate each individual symbol by a space; type \"END\" to finish inputting\n> ";
                std::vector<std::string> input_sequence;
                do
                {
                    _in >> intermediate_input;
                    input_sequence.push_back(intermediate_input);
                } while (intermediate_input != "END");
                input_sequence.pop_back();
                _out << "Production string: " + _parser.Parse(input_sequence) << std::endl;
                _out << "Done" << std::endl;
                break;
            }
        }
        catch (...)
        {
            HandleExceptionPtr(std::current_exception());
        }
    }

    return *this;
}

hrv::ui::Interface::~Interface()
{
    utils::SafeDelete(_main_menu);
    utils::SafeDelete(_read_menu);
    utils::SafeDelete(_display_grammar_menu);
    utils::SafeDelete(_display_fa_menu);
}
