#include "my_converter.h"
#include <algorithm>
#include "utils.h"

std::pair<std::string, std::string> hrv::compiler::MyConverter::SplitProductionRHS(hrv::compiler::Grammar& grammar, const hrv::compiler::ProductionExpression & expr)
{
    std::pair<std::string, std::string> res("", "");

    if (expr.IsEpsilon())
    {
        res.second = EPSILON;
        return res;
    }

    if (expr.rhs.size() == 1) // production is of the form A -> a
    {
        for (auto non_term : grammar.GetNonTerminals()) // check if RHS is a non-terminal
        {
            if (non_term.val == expr.rhs[0])
            {
                res.first = expr.rhs[0];
                return res;
            }
        }
        res.second = expr.rhs[0]; // if we get here, RHS of production is a terminal
        return res;
    }
    
    // production is of the form A -> aB or A -> Ba
    std::string first_elem = expr.rhs[0];
    std::string second_elem = expr.rhs[1];
    for (auto non_term : grammar.GetNonTerminals())
    {
        if (non_term.val == first_elem) // A -> Ba
        {
            res.first = first_elem;
            res.second = second_elem;
            return res;
        }
    }

    // A -> aB
    res.first = second_elem;
    res.second = first_elem;
    return res;
}

std::vector<std::vector<std::string>> hrv::compiler::MyConverter::CreateProductionRHS(const hrv::compiler::Automaton& automaton, const std::string terminal, const hrv::compiler::Automaton::TransitionRes & states)
{
    std::vector<std::vector<std::string>> res;

    auto& final_states = automaton.GetFinalStates();
    for (auto state : states)
    {
        if (std::find_if(final_states.begin(), final_states.end(), [&state](State final_state)
            {
                return final_state.val == state.val;
            }) != final_states.end()) // if current state is also a final state, add a production that only contains the terminal
        {
            std::vector<std::string> tmp;
            tmp.push_back(terminal);
            res.push_back(tmp);
        }
        std::vector<std::string> tmp;
        tmp.push_back(terminal);
        tmp.push_back(state.val);
        res.push_back(tmp);
    }

    return res;
}

std::string hrv::compiler::MyConverter::RandomSymbol(Grammar& grammar)
{
    size_t name_size = 1;
    std::string name;
    bool found = false;
    do
    {
        name = utils::RandomStr(name_size);
        std::for_each(grammar.GetNonTerminals().cbegin(), grammar.GetNonTerminals().cend(), [&name_size, &name, &found](auto non_term)
        {
            if (non_term.val == name)
            {
                found = true;
                ++name_size;
            }
        });
        std::for_each(grammar.GetTerminals().cbegin(), grammar.GetTerminals().cend(), [&name_size, &name, &found](auto term)
        {
            if (term == name)
            {
                found = true;
                ++name_size;
            }
        });
    } while (found);

    return name;
}

hrv::compiler::MyConverter::MyConverter()
{}

hrv::compiler::Automaton hrv::compiler::MyConverter::RegularGrammarToAutomaton(hrv::compiler::Grammar & grammar)
{
    if (!grammar.IsRegular())
        throw std::runtime_error("Cannot convert grammar to finite automaton because the grammar is not regular");

    Automaton automaton;

    // states (including initial state which may be final)
    std::for_each(grammar.GetNonTerminals().cbegin(), grammar.GetNonTerminals().cend(), [&grammar, &automaton](auto non_term)
    {
        State s;
        s.val = non_term.val;
        s.is_initial_state = non_term.is_starting_symbol;

        s.is_final_state = non_term.is_starting_symbol ? grammar.StartingSymbolHasEpsilonProduction() : false; // if starting symbol, and if starting symbol has epsilon production, add it as one of the final states, otherwise current state is not final
        automaton.AddState(s);
    });

    // new final state
    State final_state;
    final_state.is_final_state = true;
    final_state.is_initial_state = false;
    final_state.val = RandomSymbol(grammar);
    automaton.AddState(final_state);

    // alphabet
    std::for_each(grammar.GetTerminals().cbegin(), grammar.GetTerminals().cend(), [&automaton](auto term)
    {
        automaton.AddSymbolToAlphabet(term);
    });

    // transitions
    for (auto& expr : grammar.GetProductions())
    {
        if (expr.IsEpsilon())
            continue;
        Automaton::TransitionArg argument;
        argument.first  = expr.lhs;
        Automaton::TransitionRes states;

        // non-terminal to the left, terminal to the right
        std::pair<std::string, std::string> split_rhs = SplitProductionRHS(grammar, expr);
        argument.second = split_rhs.second;

        // if production is A -> aB or A -> Ba
        if (split_rhs.first != "") 
            states.insert(split_rhs.first);
        else // production is A -> a
            states.insert(final_state.val);

        automaton.AddTransition(argument, states);
    }

    return automaton;
}

hrv::compiler::Grammar hrv::compiler::MyConverter::FiniteAutomatonToRegularGrammar(hrv::compiler::Automaton & automaton)
{
    Grammar grammar;

    // non-terminals + starting symbol
    for (auto& state : automaton.GetStates())
    {
        Symbol non_terminal;
        non_terminal.is_starting_symbol = state.is_initial_state;
        non_terminal.val = state.val;

        grammar.AddNonTerm(non_terminal);
    }

    // terminals
    for (auto& alphabet_symbol : automaton.GetAlphabet())
        grammar.AddTerm(alphabet_symbol);

    // productions
    for (auto it = automaton.GetTransitionFn().cbegin(); it != automaton.GetTransitionFn().cend(); ++it)
    {
        // delta(state, symbol) = { states };
        const Automaton::TransitionArg& argument = it->first; // (state, symbol)
        const Automaton::TransitionRes& states = it->second; // { states }

        ProductionExpression expr;
        expr.lhs = argument.first.val;
        expr.operation = OP_IMPLIES;
        std::vector<std::vector<std::string>> rhs = CreateProductionRHS(automaton, argument.second, states); // create a single rhs string for all the productions; individual productions are separated by |
        for (auto single_rhs : rhs)
        {
            expr.rhs = single_rhs;
            grammar.AddProduction(expr);
        }
    }

    // if initial state is also a final state, add an epsilon production for it
    const State& initial_state = automaton.GetInitialState();
    if (initial_state.is_final_state)
    {
        ProductionExpression expr;
        expr.lhs = initial_state.val;
        expr.operation = OP_IMPLIES;
        expr.rhs.push_back(EPSILON);
        grammar.AddProduction(expr);
    }

    return grammar;
}

hrv::compiler::MyConverter::~MyConverter()
{}
