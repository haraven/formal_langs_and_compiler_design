#pragma once
#include <set>
#include <string>
#include <boost\multi_index_container.hpp>
#include <boost\multi_index\ordered_index.hpp>
#include <boost\multi_index\random_access_index.hpp>
#include <boost\multi_index\identity.hpp>
#include "file_reader.h"

namespace hrv
{
    using namespace io;
    namespace compiler
    {
        template <typename T>
        using boost_set = boost::multi_index_container<
            T,
            boost::multi_index::indexed_by<
            boost::multi_index::random_access<>,
            boost::multi_index::ordered_unique<boost::multi_index::identity<T>>
            >
        >;

        enum GrammarExpressionOperation
        {
            OP_IMPLIES = 0,
            OP_OR,
            OP_NONE
        };

        class Symbol
        {
        public:
            Symbol() {}
            Symbol(const std::string& val, const bool& is_starting_symbol = false)
                : val(val)
                , is_starting_symbol(is_starting_symbol)
            {}

            friend bool operator<(const hrv::compiler::Symbol& left, const hrv::compiler::Symbol& right);

            std::string ToString() const;

            std::string val;
            bool is_starting_symbol;
        };

        class ProductionExpression
        {
        public:
            ProductionExpression() {}
            ProductionExpression(const std::string lhs, const GrammarExpressionOperation& op, const std::vector<std::string>& rhs)
                : lhs(lhs)
                , operation(op)
                , rhs(rhs)
            {}

            friend bool operator<(const hrv::compiler::ProductionExpression& l, const hrv::compiler::ProductionExpression& r);
            bool operator==(const hrv::compiler::ProductionExpression& ot);
            bool IsEpsilon() const;
            bool RHSContains(std::string elem) const;

            std::string ToString() const;

            std::string lhs;
            GrammarExpressionOperation operation;
            std::vector<std::string> rhs;
        };

        const static std::string EPSILON = "epsilon";
        class Grammar
        {
        private:
            bool IsNonTermLHSTermRHS(const hrv::compiler::ProductionExpression& expr);
            bool RightIsNonTermLHSCompositeRHS(const hrv::compiler::ProductionExpression& expr);
            bool LeftIsNonTermLHSCompositeRHS(const hrv::compiler::ProductionExpression& expr);
            bool StartingSymbolHasEpsilonProductionAndIsOnRHS() const;
            std::string RandomSymbol() const;
            bool ContainsOnlyTerms(const ProductionExpression& expr) const;
            
        public:
            Grammar();

            hrv::compiler::Grammar& AddNonTerm(const Symbol& nonterm);
            hrv::compiler::Grammar& AddNonTerms(const std::set<Symbol>& nonterms);
            hrv::compiler::Grammar& AddTerm(const std::string term);
            hrv::compiler::Grammar& AddTerms(const std::set<std::string>& terms);
            hrv::compiler::Grammar& AddProduction(const ProductionExpression& expr);
            hrv::compiler::Grammar& AddProductions(const boost_set<ProductionExpression>& expressions);
            hrv::compiler::Grammar& Clear();

            bool ContainsStartingSymbol() const;
            bool IsNonTerminal(const std::string& symbol) const;
            bool IsTerminal(const std::string& symbol) const;
            const std::set<Symbol>& GetNonTerminals() const { return _non_terminals; }
            const Symbol& GetStartingSymbol() const;
            const std::set<std::string>& GetTerminals() const { return _terminals; }
            const boost_set<ProductionExpression>& GetProductions() const { return _productions; }
            boost_set<ProductionExpression> GetProductionsOf(const std::string& non_term) const;
            bool StartingSymbolHasEpsilonProduction() const;

            friend class GrammarHelper;

            hrv::compiler::Grammar& operator=(const hrv::compiler::Grammar& ot);

            bool IsRightRegular();
            bool IsLeftRegular();
            bool IsRegular();
            bool IsContextFree();

            std::string ToString() const;

            ~Grammar();
        private:
            std::set<Symbol> _non_terminals;
            std::set<std::string> _terminals;
            boost_set<ProductionExpression> _productions;
        };

        class GrammarReader : public FileReader
        {
        private:
            std::string SimplifyLine(std::string line);
            std::vector<std::vector<std::string>> ParseProductionRHS(const std::string& rhs);

            void HandleLines(Iterator& line_it, Grammar& g);
            void HandleNonTerminal(const std::string& line, Grammar& g);
            void HandleTerminal(const std::string& line, Grammar& g);
            void HandleProduction(const std::string& line, Grammar& g);

            hrv::compiler::Symbol HandleNonTerminal(const std::string& line);
            std::set<std::string> HandleTerminals(const std::string& line);
            boost_set<hrv::compiler::ProductionExpression> HandleProductions(const std::string& line);
        public:
            GrammarReader(const std::string& filename = NO_FILE);

            hrv::compiler::Grammar GetGrammar();
            std::set<hrv::compiler::Symbol> GetNonTerminalsFromStream(std::istream& in);
            std::set<std::string> GetTerminalsFromStream(std::istream& in);
            boost_set<hrv::compiler::ProductionExpression> GetProductionsFromStream(std::istream& in);
        private:
            std::string _non_term_indicator;
            std::string _term_indicator;
            std::string _production_indicator;
            std::string _finished_indicator;
        };

        class GrammarHelper
        {
        public:
            static hrv::compiler::Grammar EliminateInaccesibleSymbols(const hrv::compiler::Grammar& g);
        };
    }
}
