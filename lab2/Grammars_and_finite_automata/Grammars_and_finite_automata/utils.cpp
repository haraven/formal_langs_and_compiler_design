#include "utils.h"
#include <ctime>

std::vector<std::string> hrv::utils::Split(const std::string &s, char delim)
{
    std::vector<std::string> elems;
    Split(s, delim, elems);

    return elems;
}

std::vector<std::string> hrv::utils::SplitNoNeighboringDelims(const std::string & s, char delim)
{
    std::vector<std::string> res;

    std::string tmp;
    for (size_t i = 0; i < s.size(); ++i)
        if (s[i] != delim)
            tmp += s[i];
        else
        {
            if (i + 1 < s.size() && s[i + 1] == delim)
            {
                tmp += delim;
                tmp += delim;
                ++i;
            }
            else if (i > 0 && s[i - 1] == delim)
                tmp += delim;
            else if (tmp.size())
            {
                res.push_back(tmp);
                tmp.clear();
            }
        }
    if (tmp.size())
        res.push_back(tmp);

    return res;
}

std::string hrv::utils::TrimCharacter(const std::string & s, char ch)
{
    std::string tmp = s;
    tmp.erase
    (
        std::remove_if
        (
            tmp.begin(),
            tmp.end(),
            [](char x)
            {
                return std::isspace(x);
            }
        ),
        tmp.end()
    );

    return tmp;
}

std::string hrv::utils::RandomStr(size_t length)
{
    static bool is_seed_initialized = false;
    static std::string charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

    if (!is_seed_initialized)
    {
        srand(static_cast<unsigned int>(time(NULL)));
        is_seed_initialized = true;
    }

    std::string result;
    result.resize(length);
    
    for (size_t i = 0; i < length; ++i)
        result[i] = charset[rand() % charset.length()];

    return result;
}

void hrv::utils::Split(const std::string &s, char delim, std::vector<std::string> &elems)
{
    std::stringstream ss;
    ss.str(s);
    std::string item;
    while (std::getline(ss, item, delim)) 
    {
        if (item == "" && s != "")
            continue;
        elems.push_back(item);
    }
}