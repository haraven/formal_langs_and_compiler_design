#include "file_reader.h"
#include <fstream>
#include <logger.hpp>

std::string hrv::io::FileReader::ReadFile(const char * separator)
{
    if (_filename != NO_FILE)
    {
        std::ifstream file(_filename);
        if (!file.is_open())
            throw std::runtime_error("Could not find file");
        std::string res = "";
        std::string tmp;
        while (std::getline(file, tmp))
            res += tmp + separator;
        file.close();
        return res;
    }

    return "";
}

hrv::io::FileReader::FileReader(const std::string & filename, const char* separator)
: _filename(filename)
, _separator(separator)
, _tokenizer(NO_FILE)
{
    _filename  = filename;
    _data      = ReadFile(separator);
    _tokenizer = boost::tokenizer<boost::char_separator<char>>(_data, _separator);
}

hrv::io::FileReader & hrv::io::FileReader::Open(const std::string & filename, const char* separator)
{
    _filename  = filename;
    _data      = ReadFile(separator);
    _separator = boost::char_separator<char>(separator);
    _tokenizer = boost::tokenizer<boost::char_separator<char>>(_data, _separator);

    return *this;
}

hrv::io::FileReader::Iterator hrv::io::FileReader::Begin()
{
    return _tokenizer.begin();
}

hrv::io::FileReader::Iterator hrv::io::FileReader::End()
{
    return _tokenizer.end();
}

hrv::io::FileReader & hrv::io::FileReader::Close()
{
    _data.clear();
    _filename = NO_FILE;

    return *this;
}

hrv::io::FileReader::~FileReader()
{
    Close();
}
